using UnityEngine;
using Vuplex.WebView;
using System;
using Bolt;  // Make sure to include the Bolt namespace

public class WebViewController : MonoBehaviour
{
    public CanvasWebViewPrefab webViewPrefab;
    public string initialURL = "https://map.hidden.no/";
    public bool startVisible = true;
    private string _lastMessage;

    async void Start()
    {
        if (webViewPrefab != null)
        {
            // Wait for the WebViewPrefab to initialize
            await webViewPrefab.WaitUntilInitialized();

            // Set the initial URL
            webViewPrefab.InitialUrl = initialURL;

            // Ensure the WebView is not visible at first
            webViewPrefab.Visible = startVisible;

            // Load the initial URL
            webViewPrefab.WebView.LoadUrl(webViewPrefab.InitialUrl);

            // Add an event listener for messages from JavaScript
            webViewPrefab.WebView.MessageEmitted += WebView_MessageEmitted;
        }
        else
        {
            Debug.LogError("webViewPrefab is not assigned.");
        }
    }

    private void WebView_MessageEmitted(object sender, EventArgs<string> e)
    {
        // Log the message received
        Debug.Log("Message received from JavaScript: " + e.Value);

        // Store the message in a class variable
        _lastMessage = e.Value;

        // Trigger a custom Bolt event with the message
        CustomEvent.Trigger(gameObject, "MessageReceived", e.Value);
    }

    public string GetLastMessage()
    {
        return _lastMessage;
    }

    [Serializable]
    private class MessageData
    {
        public string type;
        public string message;
    }

    // Method to send a message to JavaScript
    public void SendMessageToJavaScript(string type, string message)
    {
        if (webViewPrefab != null && webViewPrefab.WebView != null)
        {
            // Create a new message data object
            var messageData = new MessageData { type = type, message = message };

            // Serialize the message data to JSON format using JsonUtility
            string jsonMessage = JsonUtility.ToJson(messageData);

            // Send the JSON message to JavaScript
            webViewPrefab.WebView.PostMessage(jsonMessage);
            Debug.Log("Message sent to JavaScript: " + jsonMessage);
        }
        else
        {
            Debug.LogError("WebViewPrefab or WebView is not initialized.");
        }
    }
}
