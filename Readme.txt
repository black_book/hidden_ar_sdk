v. 0.0.7
=================
- Hidden AR SDK -
=================

To use this package, make sure you also add Black Book's Unity Utils package from git:
https://bitbucket.org/black_book/unity_utilities.git

After adding both Unity Utils and this package to the Unity Package Manager, if you get an error on missing metafiles for DLLs
try expanding the Packages/Black Book: Hidden SDK folder, right-click "External" and select "Reimport", then wait a while for Unity
to catch up.

You cannot compile your own code and run it as part of the loaded assetBundle that you upload on our servers.
You can however use BOLT to create custom logic, and you have access to some basic scripts that we have made available to you.
They are located here, in the SDK.

In addition, Hidden is also compatible to use with the following packages if you have access to them:
- Dynamic Bone
- Text Mesh Pro
- BOLT
- Yarn (make sure to import our version by double clicking the yarn.unitypackage in the 'SDK\External' folder. Any other versions of Yarn will not work, as we have modified the code to suit our needs)

The SDK can also be used as a GIT Submodule - to push and pull files here you must go through some additional steps.
- Ask you local Mattias
- Git good
