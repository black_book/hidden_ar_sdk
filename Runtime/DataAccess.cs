using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using BlackBook.Unity_Utilities;
using UnityEngine;

[Serializable]
public class ExperienceData
{
    public string ownerName { get; set; }
    public int id { get; set; }
    public string assetURL { get; set; }
    public string experienceName { get; set; }
    public string readKey { get; set; }
    public int assetVersion { get; set; }
    public string sceneOrientation { get; set; }  // Added SceneOrientation field to ExperienceData
}

public static class AWSConsts
{
    public const string DEV_AR_LOOKUP_TABLENAME = "AR_Content_Lookup-Dev";
    public const string BUCKET_NAME = "hidden-ar-solution-assetbundles";

    public static class TableKeys_AR
    {
        public const string OWNER_ID = "ownerID"; // Partition Key
        public const string UNIQUE_ID = "id";     // Sort Key
        public const string WRITE_KEY = "writeKey";
        public const string READ_KEY = "readKey";
        public const string ASSET_URL = "assetURL";
        public const string ASSET_NAME = "name";
        public const string VERSION_NUMBER = "assetVersion";
        public const string SCENE_ORIENTATION = "SceneOrientation";  // Added SceneOrientation
    }
}

public class DataAccess
{
    public static void InitializeDataAccess(string ACCESS_KEY, string SECRET_KEY)
    {
        if (client == null)
        {
            AWSCredentials cred = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
            client = new AmazonDynamoDBClient(cred, RegionEndpoint.EUCentral1);
            s3Client = new AmazonS3Client(cred, RegionEndpoint.EUCentral1);
        }
    }

    #region Properties

    private static AmazonDynamoDBClient client;
    public static AmazonDynamoDBClient Client => client;
    private static IAmazonS3 s3Client;
    public static IAmazonS3 S3Client => s3Client;

    #endregion

    #region Retrieve Experience

    public static async Task<ExperienceData> GetExperienceData(string writeKey)
    {
        var request = new ScanRequest
        {
            TableName = AWSConsts.DEV_AR_LOOKUP_TABLENAME,
            ExpressionAttributeValues = new Dictionary<string, AttributeValue>
            {
                {":val", new AttributeValue {S = writeKey}}
            },
            FilterExpression = "writeKey = :val",
        };

        ScanResponse response = await Client.ScanAsync(request);
        ExperienceData experienceData = new ExperienceData();
        if (response.Count != 0)
        {
            foreach (var VARIABLE in response.Items[0])
            {
                switch (VARIABLE.Key)
                {
                    case AWSConsts.TableKeys_AR.OWNER_ID:
                        experienceData.ownerName = VARIABLE.Value.S;
                        break;
                    case AWSConsts.TableKeys_AR.UNIQUE_ID:
                        experienceData.id = Int32.Parse(VARIABLE.Value.N);
                        break;
                    case AWSConsts.TableKeys_AR.READ_KEY:
                        experienceData.readKey = VARIABLE.Value.S;
                        break;
                    case AWSConsts.TableKeys_AR.ASSET_URL:
                        experienceData.assetURL = VARIABLE.Value.S;
                        break;
                    case AWSConsts.TableKeys_AR.ASSET_NAME:
                        experienceData.experienceName = VARIABLE.Value.S;
                        break;
                    case AWSConsts.TableKeys_AR.VERSION_NUMBER:
                        experienceData.assetVersion = Int32.Parse(VARIABLE.Value.N);
                        break;
                    case AWSConsts.TableKeys_AR.SCENE_ORIENTATION:  // Retrieve SceneOrientation
                        experienceData.sceneOrientation = VARIABLE.Value.S;
                        break;
                }
            }
        }
        else
        {
            return null;
        }

        return experienceData;
    }

    #endregion

    #region S3 Delete Functions

    public static async void DeleteAssetBundlesInSlot(List<string> keys, Action callback)
    {
        foreach (string key in keys)
        {
            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = AWSConsts.BUCKET_NAME,
                Key = key,
            };
            await DeleteOperation(deleteObjectRequest);
        }
        callback?.Invoke();
    }

    private static async Task DeleteOperation(DeleteObjectRequest deleteObjectRequest)
    {
        try
        {
            await S3Client.DeleteObjectAsync(deleteObjectRequest);
        }
        catch (AmazonS3Exception e)
        {
            Console.WriteLine("Error encountered on server. Message:'{0}' when deleting an object", e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Unknown error encountered on server. Message:'{0}' when deleting an object", e.Message);
        }
    }

    #endregion

    #region S3 Upload Functions

    public static async void UploadAssetBundlesInSlot(List<UploadContentObject> contents, Action callback)
    {
        try
        {
            foreach (UploadContentObject content in contents)
            {
                Debug.Log(content.FilePath);
                var request = new PutObjectRequest
                {
                    BucketName = AWSConsts.BUCKET_NAME,
                    Key = content.KeyName,
                    FilePath = content.FilePath,
                };
                await UploadOperation(request);
            }
            callback?.Invoke();
        }
        catch (AmazonS3Exception e)
        {
            Console.WriteLine("Error encountered. Message:'{0}' when writing an object", e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Unknown error encountered on server. Message:'{0}' when writing an object", e.Message);
        }
    }

    private static async Task UploadOperation(PutObjectRequest put)
    {
        try
        {
            PutObjectResponse response = await s3Client.PutObjectAsync(put);
        }
        catch (AmazonS3Exception e)
        {
            Console.WriteLine("Error encountered on server. Message:'{0}' when uploading an object", e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine("Unknown error encountered on server. Message:'{0}' when uploading an object", e.Message);
        }
    }

    #endregion

    #region Update DynamoDB

    public static async void UpdateURLInDynamoDB(string url, string ownerName, int id, string sceneOrientation)
    {
        UpdateItemResponse updateResponse = await UpdateOperation(url, ownerName, id, sceneOrientation);
        if (updateResponse != null)
        {
            Debug.Log("Update has been completed successfully!");
        }
    }

    private static async Task<UpdateItemResponse> UpdateOperation(string url, string ownerName, int id, string sceneOrientation)
    {
        try
        {
            var request = new UpdateItemRequest
            {
                TableName = AWSConsts.DEV_AR_LOOKUP_TABLENAME,
                Key = new Dictionary<string, AttributeValue>
                {
                    { "id", new AttributeValue { N = id.ToString() } },
                    { "ownerID", new AttributeValue { S = ownerName } }
                },
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    { "#P", "assetURL" },
                    { "#V", "assetVersion" },
                    { "#SO", "SceneOrientation" }
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    { ":p", new AttributeValue { S = url } },
                    { ":v", new AttributeValue { N = "1" } },
                    { ":so", new AttributeValue { S = sceneOrientation } }
                },
                UpdateExpression = "SET #P = :p, #V = #V + :v, #SO = :so"
            };
            var response = await client.UpdateItemAsync(request);
            return response;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    #endregion

    #region Create Attributes in DynamoDB

    public static async void CreateAttributes(string ownerName, int id, string sceneOrientation)
    {
        UpdateItemResponse response = await PutNewItems(ownerName, id, sceneOrientation);
        if (response != null)
        {
            Debug.Log("New attributes have been created in DynamoDB: 'assetURL', 'name', 'assetVersion', 'SceneOrientation'.");
        }
    }

    private static async Task<UpdateItemResponse> PutNewItems(string ownerName, int id, string sceneOrientation)
    {
        try
        {
            var request = new UpdateItemRequest
            {
                TableName = AWSConsts.DEV_AR_LOOKUP_TABLENAME,
                Key = new Dictionary<string, AttributeValue>
                {
                    { "id", new AttributeValue { N = id.ToString() } },
                    { "ownerID", new AttributeValue { S = ownerName } }
                },
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    { "#P", "assetURL" },
                    { "#V", "assetVersion" },
                    { "#NA", "name" },
                    { "#SO", "SceneOrientation" }
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    { ":p", new AttributeValue { S = "" } },
                    { ":n", new AttributeValue { S = "" } },
                    { ":v", new AttributeValue { N = "0" } },
                    { ":so", new AttributeValue { S = sceneOrientation } }  // Set default SceneOrientation
                },
                UpdateExpression = "SET #P = :p, #V = :v, #NA = :n, #SO = :so"
            };
            var response = await client.UpdateItemAsync(request);
            return response;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    #endregion
}
