using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UploadContentObject
{
    public string FilePath { get; set; }
    public string KeyName { get; set; }
    public string BucketName { get; set; }
    public Dictionary<string, string> Metadata { get; set; }

    public UploadContentObject()
    {
        Metadata = new Dictionary<string, string>();
    }
}
