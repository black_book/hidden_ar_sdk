using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrainCheck {

	public class LocationManagerBridge : MonoBehaviour  {
	    static AndroidJavaClass _class;
		static AndroidJavaObject instance { get { return _class.GetStatic<AndroidJavaObject>("instance"); } }

		private static void SetupPlugin () {
			if (_class == null) {
				_class = new AndroidJavaClass ("com.plugin.locationserviceandroid.LocationServicePlugin");
				_class.CallStatic ("_initiateFragment");
				_class.CallStatic ("_addFragmentToActivity");
			}
		}

		private static void AddFragmentToActivity () {
			SetupPlugin ();
		}

		//----------------------------------------------Start Location--------------------------------------------------------------
		public static void StartLocationMonitoring(){
			SetupPlugin ();
		   	instance.Call("_startLocationMonitoring");
		}

		//----------------------------------------------Stop Location--------------------------------------------------------------
		public static void StopLocationMonitoring(){
			SetupPlugin ();
		   	instance.Call("_stopLocationMonitoring");
		}

		//------------------------------------------------------------------------------------------------------------
		public static void GetLocationAuthorization(){
			SetupPlugin ();
		   	instance.Call("_checkLocationPermission");
		}

		//------------------------------------------------------------------------------------------------------------
		public static void RequestRunTimeLocationPermission(){
			SetupPlugin ();
		   	instance.Call("_requestRunTimeLocationPermission");
		}

		//------------------------------------------------------------------------------------------------------------

		public static void OpenSettingsForLocationPermission(){
			SetupPlugin ();
		   	instance.Call("_openSettingsForLocationPermission");
		}

		//------------------------------------------------------------------------------------------------------------
		public static void SetUnityGameObjectNameAndMethodName(string gameObject, string methodName){
			SetupPlugin ();
		   	instance.Call("_setUnityGameObjectNameAndMethodName", gameObject, methodName);
		}

		//----------------------------------------------Open Google Maps--------------------------------------------------------------
		public static void OpenGoogleMapWithCurrentLocation(){
			SetupPlugin ();
		   	instance.Call("_openGoogleMapWithCurrentLocation");
		}

		//----------------------------------------------Open Google Maps--------------------------------------------------------------
		public static void OpenGoogleMapWithCustomLocation(string latitude, string longitude){
			SetupPlugin ();
		   	instance.Call("_openGoogleMapWithLocation", latitude, longitude);
		}

		//----------------------------------------------Get Current Location--------------------------------------------------------------
		public static void GetCurrentLocation(){
			SetupPlugin ();
		   	instance.Call("_getLatLong");
		}
	}
}

