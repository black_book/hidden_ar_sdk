﻿using UnityEngine;
using BlackBook.Hidden.SDK;

namespace BlackBook.Hidden.Core.ARUX
{
    [CreateAssetMenu(fileName = "Scene Info Data", menuName = "Black Book/New Scene Content File", order = 1)]
    public class UXContentObject : ScriptableObject
    {
        [TextArea(3, 100)]
        public string Preamble = "This is the Preamble text, below the image. It is always visible as long as the creature is not placed.";
        public Sprite Image;
        public string Header = "Tips";
        [TextArea(3, 100), Tooltip("Write \"NoContent\" without quotation marks to hide the body section completly")]
        public string Body = "Here goes the tips";
        public bool UseCustomGuiColor = false;
        public Color GUIColor = Color.black;
        public Color TextColor = Color.white;
        [Tooltip("Optional watermark")] public Sprite Watermark;
        public bool HideSideDeck = false;
        public bool HideResetButton = false;
    }
}