﻿//AUthor Mattias Tronslien 2019 mntronslien@gmail.com
//For use in the AR UX instruction scene
using BlackBook.Unity_Utilities;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using BlackBook.Hidden.SDK;

namespace BlackBook.Hidden.Core.ARUX
{

    public class UXController : ICareAboutTracking
    {
        [Header("Side Deck")]
        public float _SideDeckAnimationSpeed = 0.5f;
        public RectTransform _Canvas;
        public RectTransform _Sidedeck;
        public RectTransform _Peaker;
        public UIState UXCurrentState;
        //public bool SideDeckSlideIsRunning = false;
        public AnimationCurve SlideMotion = AnimationCurve.Linear(0, 0, 1, 1);
        private bool HideSideDeck = false;
        private bool hideResetButton = false;

        [Header("Other UI Elements")]
        public Image _DrawerHandleImage;
        public Image _exitButton;
        public Image _exitButtonArrow;
        public CanvasGroup _Buttons;
        public Image _ResetCreaturePlacementButton;
        // public Image _CameraButton;
        //public CameraButton _cameraButton;
        public bool _AutoShowCameraWhenTracking;

        [Header("Behavior")]
        public float _GracePeriodOnLostCreature;
        public float _TimeSpentLost;
        public bool _HaveLostCreature;

        [Header("Content Load")]
        public Image Image;
        public TMP_Text PreambleAlt;
        public TMP_Text Preamble;
        public TMP_Text Header;
        public TMP_Text Body;
        private Sprite Watermark;

        [Header("Custom formatting")]
        public Image peaker;
        public Image drawerGradient;
        public Image body;

        private WatermarkController watermarkController;

        [Header("Data"), SerializeField]
        UXContentObject Data;

        //thomasw
        //Back button the UI is available if it know where to go back to, this i can remove bacuse it always know swhere to go back to by referenving the systemmanager - Mattias
        //  void UpdateExitButtonAvailability()
        //  {
        //
        //      _exitButton?.gameObject.SetActive(ManagerDeepLinking.Instance.HasReceivedCallFromHiddenMotherApp());
        //  }


        //Scene gets reset i think, unsure where this gets called from
        public void OnResetPressed()
        {
            LoadContent();
            if (UXCurrentState == UIState.Open) return;
            //if (HiddenPlaneManager.Instance == null) return;

            // TODO: Integrate whever it is sindre is working on
            // HiddenPlaneManager.Instance.ResetScene();
            // HiddenPlaneManager.Instance.ResetTrackers();
        }

        void Start()
        {
            watermarkController = GetComponent<WatermarkController>();

            //Disable UI? ToggleUI.Instance?.EnableUI(false);

            //SUBSCRIBE LISTENERS
            //TODO: Hook listeners up to what sindre makes with the ground planes

            //ManagerAR.OnCreatureSpawned += OnCreatureSpawned;
            //ManagerAR.OnCreatureDespawned += OnCreatureDespawned;

            //On spawn, reset to known defaults
            HardResetSideDeck();
            if (!HideSideDeck)
                SetUIState(UIState.Closed);
            else
                SetUIState(UIState.Hidden);

            //The UI was designed to work with ground planes and with image target, but the UI does change in behavious slightly when this happens,
            //for now i will override this to work with my use case and enable image tracking after wards


            //LoadContent();

            //TODO:
            //Set up local back behaviour to return to main menu as this is UI for a simple scene
            //BlackBook.Hidden.Core.SytemManager.BackButtonOverride() = Define local method; USE LAMBDA TO LOAD MAIN MENU
        }
        public void LoadContent(UXContentObject newData, bool showReset)
        {
            Data = newData;

            if (!hideResetButton)
                _ResetCreaturePlacementButton.gameObject.SetActive(showReset);
            LoadContent();
        }

        [Sabotender.ExposeInEditor]
        public void LoadContent()
        {
            //UXContentObject data = GameObject.Find("SceneConfiguration").GetComponent<UXContent>().Content;
            //data = UXContent.data;
            if (Data != null)
            {
                //TODO: Extract formatting of UI into its own Format() method based on the loaded data
                print(Data.Preamble);
                Preamble.text = Data.Preamble;
                Header.text = Data.Header;
                Body.text = Data.Body;

                Watermark = Data.Watermark;
                if (Watermark != null)
                {
                    watermarkController.SetWatermark(Watermark);
                    watermarkController.ShowWatermark(true);
                }
                else
                    watermarkController.ShowWatermark(false);
                {

                }
                if (Data.Image != null)
                {
                    VerticalLayoutGroup layout = Image.transform.parent.GetComponent<VerticalLayoutGroup>();
                    layout.childForceExpandHeight = false;
                    layout.childControlHeight = false;
                    Image.gameObject.SetActive(true);
                    Preamble.gameObject.SetActive(true);
                    PreambleAlt.gameObject.SetActive(false);
                    Image.sprite = Data.Image;
                }
                else
                {
                    VerticalLayoutGroup layout = Image.transform.parent.GetComponent<VerticalLayoutGroup>();
                    layout.childForceExpandHeight = true;
                    layout.childControlHeight = true;
                    Image.gameObject.SetActive(false);
                    Preamble.gameObject.SetActive(false);
                    PreambleAlt.gameObject.SetActive(true);
                    PreambleAlt.text = Data.Preamble;
                    //ChildForce expand in layout vertical group
                }

                if (Data.UseCustomGuiColor)
                {
                    //Set color of GUI
                    peaker.color = Data.GUIColor;
                    drawerGradient.color = Data.GUIColor;
                    body.color = Data.GUIColor;

                    Header.color = Data.TextColor;
                    Body.color = Data.TextColor;
                    Preamble.color = Data.TextColor;
                    PreambleAlt.color = Data.TextColor;

                } //TODO: set default color

                if (Data.HideSideDeck)
                    HideSideDeck = true;

                if (Data.HideResetButton)
                { 
                    hideResetButton = true;
                    _ResetCreaturePlacementButton.gameObject.SetActive(hideResetButton);
                }
                    

                //If the body of the event has "NOContent" as string, that means it has no content and therefore should disable the functionallity.
                //Currently we disable this by removing the button to open it, it is still there but invissble to the EndUser
                _DrawerHandleImage.gameObject.SetActive(!Data.Body.Equals("NoContent"));

                return;
            }
            else _DrawerHandleImage.gameObject.SetActive(false);
            string s = "You tried to load data from a UX content obejct, but the UI did not have any UXcontent object assigned.\n" +
                "Use the LoadContent(UxContentObject) to assign and load content";
            Toast.Show(s);
            Debug.LogWarning(s);

        }

        private void OnDestroy()
        {
            //UNSUBSCRIBE LISTENERS
            // ManagerAR.OnCreatureSpawned -= OnCreatureSpawned;
            // ManagerAR.OnCreatureDespawned -= OnCreatureDespawned;


        }


        private void Update()
        {
            if (_HaveLostCreature && !HideSideDeck)
            {
                if (_TimeSpentLost > _GracePeriodOnLostCreature && UXCurrentState == UIState.Hidden)
                {
                    //Tell player how to find creature (SHOW INFO AGAIN)
                    SetUIState(UIState.Closed);
                }
                else _TimeSpentLost += Time.unscaledDeltaTime;
            }

            if (HideSideDeck && UXCurrentState != UXController.UIState.Hidden)
                SetUIState(UIState.Hidden);

        }
        //--------------------------------------------------------//

        private Coroutine activeRoutine;
        //private IEnumerator SideDeckRoutine;
        //[Sabotender.ExposeInEditor]
        public void ToggleSideDeck()
        {
            if (activeRoutine == null)
            {
                switch (UXCurrentState)
                {
                    case UIState.Closed:
                        activeRoutine = StartCoroutine(AnimateUI(UIState.Open));
                        break;
                    case UIState.Open:
                        activeRoutine = StartCoroutine(AnimateUI(UIState.Closed));
                        break;
                    case UIState.Hidden:
                        Debug.LogWarning("SideDeck is Hidden, no change of position", this);
                        break;
                }
            }
        }

        //[Sabotender.ExposeInEditor]
        public void ToggleSideDeckHidden()
        {
            if (activeRoutine == null)
            {
                switch (UXCurrentState)
                {
                    case UIState.Closed:
                    case UIState.Open:
                        activeRoutine = StartCoroutine(AnimateUI(UIState.Hidden));
                        break;
                    case UIState.Hidden:
                        activeRoutine = StartCoroutine(AnimateUI(UIState.Closed));
                        break;
                }
            }
        }

        //[Sabotender.ExposeInEditor]
        public void SetUIState(UIState newState)
        {
            if (activeRoutine != null)
            {
                StopCoroutine(activeRoutine);

            }
            activeRoutine = StartCoroutine(AnimateUI(newState));
        }

        private IEnumerator AnimateUI(UIState targetState)
        {
            print("UI Animation routine initialized. Target position is: " + targetState);
            UIState OldPos = UXCurrentState;
            UXCurrentState = targetState;
            //Animate deck

            float startValue = _Sidedeck.anchoredPosition.x;
            float endValue = 0;
            Color backButtStart = _exitButton.color;
            Color backButEnd = backButtStart;
            Color backButtArrowStart = _exitButtonArrow.color;
            Color backButtArrowEnd = backButtArrowStart;


            //TODO: Use canvas groups instead of changing alphas
            switch (targetState)
            {
                case UIState.Closed:
                    endValue = _Canvas.rect.width * (1 - _Peaker.anchorMax.x);
                    _DrawerHandleImage.rectTransform.localScale = new Vector2(1, 1);
                    backButEnd.a = 1;
                    backButtArrowEnd.a = 1;

                    FadeImageIterative(_Buttons, 0, _SideDeckAnimationSpeed);
                    watermarkController.ShowWatermark(false);


                    break;
                case UIState.Open:
                    if (Data.Body.Equals("NoContent"))
                    {
                        //SideDeckSlideIsRunning = false;
                        UXCurrentState = OldPos;
                        print("UX side deck aborted because the target position is illegal");
                        activeRoutine = null;
                        yield break;
                    }

                    endValue = 0;
                    _DrawerHandleImage.rectTransform.localScale = new Vector2(-1, 1);
                    backButEnd.a = 0;
                    backButtArrowEnd.a = 0;

                    FadeImageIterative(_Buttons, 0, _SideDeckAnimationSpeed);
                    watermarkController.ShowWatermark(false);



                    break;
                case UIState.Hidden:
                    endValue = _Canvas.rect.width;
                    _DrawerHandleImage.rectTransform.localScale = new Vector2(1, 1);
                    backButEnd.a = 1;
                    backButtArrowEnd.a = 1;
                    FadeImageIterative(_Buttons, 1, _SideDeckAnimationSpeed);
                    if (Watermark != null) watermarkController.ShowWatermark(true);

                    break;
            }
            float t = 0;
            Vector2 v2 = Vector2.zero;
            while (t <= _SideDeckAnimationSpeed)
            {
                v2.x = Mathf.Lerp(startValue, endValue, SlideMotion.Evaluate(t / _SideDeckAnimationSpeed));
                _Sidedeck.anchoredPosition = v2;

                _exitButton.color = Color.Lerp(backButtStart, backButEnd, t / _SideDeckAnimationSpeed);
                _exitButtonArrow.color = Color.Lerp(backButtArrowStart, backButtArrowEnd, SlideMotion.Evaluate(t / _SideDeckAnimationSpeed));

                t += Time.deltaTime;
                yield return null;
            }
            v2.x = endValue;
            _Sidedeck.anchoredPosition = v2;
            //SideDeckSlideIsRunning = false;
            activeRoutine = null;
        }

        private void HardResetSideDeck()
        {
            Vector2 v2 = Vector2.zero;
            v2.x = _Canvas.rect.width;
            _Sidedeck.anchoredPosition = v2;
            _DrawerHandleImage.rectTransform.localScale = new Vector2(1, 1);
        }

        public enum UIState
        {
            Closed, //Vissible help text
            Open, //Full text on screen
            Hidden //Compleatly hidden for the user 
        }


        public IEnumerator FadeImage(CanvasGroup Group, float newAlpha, float duration)
        {

            float t = 0;
            float startAlpha = Group.alpha;
            while (t <= duration)
            {
                Group.alpha = Mathf.Lerp(startAlpha, newAlpha, SlideMotion.Evaluate(t / duration)); //I only want the motion of the curve, not the values
                Group.interactable = Group.alpha > 0.5f;
                t += Time.deltaTime;
                yield return null;
            }
            Group.alpha = newAlpha;
        }


        public void FadeImageIterative(CanvasGroup img, float newAlpha, float duration)
        {
            StartCoroutine(FadeImage(img, newAlpha, duration));
        }

        #region Listeners
        //===========//
        // LISTENERS //
        //===========//
        public override void AR_HandleImageFound(ARTrackedImage image)
        {
            DoCreaturePlaced();
        }
        public override void AR_HandleImageRemoved(ARTrackedImage image)
        {
            DoLookForCreature();
        }
        public override void AR_PlacedObjectAdded(GameObject obj)
        {
            DoCreaturePlaced();
        }
        public override void AR_PlacedObjectLost(GameObject obj)
        {
            DoLookForCreature();
        }

        #endregion
        
        [Sabotender.ExposeInEditor]
        public void DoLookForCreature()
        {
            if (!_HaveLostCreature)
            {
                _HaveLostCreature = true;
                _TimeSpentLost = 0;
                Toast.Show("Creature have been LOST. UI and is showing you how it looks when it is looking for a creature");
            }
            else Toast.Show("Nothing happened. Creature already lost.");
        }
        [Sabotender.ExposeInEditor]
        public void DoCreaturePlaced()
        {
            string s = "Creature has been \"Found\"\n";
            _HaveLostCreature = false;

            if (UXCurrentState != UIState.Hidden)
            {
                SetUIState(UIState.Hidden);
                s += "UI state was not HIDDEN, now it is";
            }
            else s += "UI state was already HIDDEN, so nothing else happened.";

            Toast.Show(s);

        }

    }
}
