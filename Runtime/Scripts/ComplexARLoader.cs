using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using static BlackBook.Hidden.SDK.ArRuntimeLibraryBuilder;
/// <summary>
///Author Mattias Tronslien
///This scripts structeres a linear experience with multiple markers spread out over a large area
/// </summary>
namespace BlackBook.Hidden.SDK
{
    [RequireComponent(typeof(ARTrackedImageManager))]
    [RequireComponent(typeof(ARAnchorManager))]
    [AddComponentMenu("Black Book/Complex AR Loader")]
    public class ComplexARLoader : ICareAboutTracking
    {
        [Tooltip("")]
        public StepObject[] StepList;

        [Tooltip("Named Variables are saved in memory with a prefix. This prefix is the same for one experience, but needs to be unique across all of the hidden experiences. Recomended value is the public read key of the experience")]
        public string DataPrefix; //Example: For Hundorp this is 9704581d6064593b508c89f50b0912d6
        private string PlayerPrefVariableName = "StepProgress";


        [Header("Events")]
        public bool overrideNotYetMessage = false;
        [Tooltip("This event overrides the system toast as a feedback to the user, if they try to open a step they have not yet reached. Only overrides if the above box is checked")]
        public UnityEvent NotYetMessage;

        [Tooltip("Use this if you want behaviour (like a bolt script) to do something everytime an image is detected")]
        public UnityEvent OnImageDetected;

        [Header("PlaceAnchor fine tuning")]
        public float thresholdTime = 30;
        public float thresholdDistance = 0.05f;
        public float thresholdAngle = 3;
        public float lerpStrenght = 0.25f;

        [Header("Required References")]
        public ARSession _ARSession;

        //under the hood
        //private Experience activeStep = null;
        private GameObject activeStepGameObject;
        private StepObject activeStep = null;
        private bool doCheckForExperienceCompleted = false;
        ARSessionOrigin SessionOrigin;

        //Debug
        float debug_timeOfLoad = 0;
        bool debug_lookForMarker = true;
        string debug_imagesInLibrary = "";
        float debug_timeSpentConverged = 0;

        //Placement fine tuning
        private float markerDistance = 0;
        private float markerAngle = 0;

        [SerializeField]
        private bool useOptimizedCode = false;

        [System.Serializable]
        public class StepObject
        {
            [Tooltip("The images you want to detect. Will be added to a Reference Image Library at runtime.")]
            public ImageMarkerObject[] marker;
            [Tooltip("When an image is detected, a prefab is instanciated based on this list using the index given in the dictionary.")]
            public GameObject prefab;
            [Tooltip("Example: What level must the experience progress be at to open this level")]
            public int stepLevelRequirement;
            [Tooltip("Should this step also progress the level?")]
            public bool incrementStepProgressOnStepComplete = true;
        }
        // Start is called before the first frame update
        void Start()
        {
            #region Initialization
            Unity_Utilities.Toast.Show($"Complex AR Loader started");

            debug_timeOfLoad = Time.time;

            if (_ARSession == null) _ARSession = FindObjectOfType<ARSession>();
            if (_ARSession == null) Unity_Utilities.Toast.Show("No AR session in the scene, Complex AR Loader script will not work");
            #endregion

            GetProgress();


            #region Build_Marker_Library
            //BUILD THE MARKER LIBRARY
            int markersInTotal = 0;
            foreach (StepObject e in StepList)
            {
                markersInTotal += e.marker.Length;
            }
            ImageMarkerObject[] imageMarkers = new ImageMarkerObject[markersInTotal];
            int i = 0;
            foreach (StepObject e in StepList)
            {
                foreach (ImageMarkerObject marker in e.marker)
                {
                    imageMarkers[i] = marker;
                    i++;
                }
            }


            //Build the library      
            BuildNewLibrary(ImageManager, imageMarkers, () =>
            {
                Unity_Utilities.Toast.Show($"Library is complete and ready for use. It has {ImageManager.referenceLibrary.count} of {StepList.Length} image markers.");
                for (int i = 0; i < ImageManager.referenceLibrary.count; i++)
                {
                    //Unity_Utilities.Toast.Show($"Image [{i}]: {ImageManager.referenceLibrary[i].name} is in the Library");
                    debug_imagesInLibrary += $"Image [{i}]: {ImageManager.referenceLibrary[i].name}\n";
                }
                // From what i understand if the manager has no library, it will disable itself until it has because the library is not allowed to be null. 
                // Therefore we must enable manager when done in case it got or was disabled.
                ImageManager.enabled = true;

                //
                //When the image marker library does not atch the number of image markers in complex Ar loader, then something has gone wrong. When this happens we want to regnerate the mage library to try and solve the issue.
                //This will not be a recursive proceure, because i do not want the app to hang if this happens to not be coded correctly.
                // TODO: Make recursive but with max depth

                // Number Of Images That Should Be In The Library
                if (markersInTotal != ImageManager.referenceLibrary.count)
                {
                    debug_imagesInLibrary = "";
                    BuildNewLibrary(ImageManager, imageMarkers, () =>
                    {
                        Unity_Utilities.Toast.Show($"Library initially failed to build correctly. It is now complete and ready for use. It has {ImageManager.referenceLibrary.count} of {StepList.Length} image markers.");
                        for (int i = 0; i < ImageManager.referenceLibrary.count; i++)
                        {
                            //Unity_Utilities.Toast.Show($"Image [{i}]: {ImageManager.referenceLibrary[i].name} is in the Library");
                            debug_imagesInLibrary += $"Image [{i}]: {ImageManager.referenceLibrary[i].name}\n";
                        }
                        // From what i understand if the manager has no library, it will disable itself until it has because the library is not allowed to be null. 
                        // Therefore we must enable manager when done in case it got or was disabled.
                        ImageManager.enabled = true;

                    });
                }

            });
            #endregion

        }
        #region Debug_Overlay
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        bool showDebug = true;
        private void OnGUI()
        {
            float marginX = Screen.width * 0.2f;
            float marginY = Screen.height * 0.2f;

            Rect r = new Rect();
            r.height = Screen.height - marginY;
            r.width = (Screen.width) - marginX;
            r.position = new Vector2(marginX, marginY);

            GUILayout.BeginArea(new Rect(r));

            if (showDebug)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Reset Level Progress", GUILayout.Height(Screen.height * 0.2f))) ResetSavedVariables();
                if (GUILayout.Button("Increase Level Progress", GUILayout.Height(Screen.height * 0.2f))) IncrementStepProgress();
                if (GUILayout.Button("Decrease Level Progress", GUILayout.Height(Screen.height * 0.2f))) IncrementStepProgress(-1);
                if (activeStep != null)
                {
                    if (GUILayout.Button("Force Current Complete Step", GUILayout.Height(Screen.height * 0.2f))) CheckLevelCleanup(); StepObjectCleanup();
                }
                if (GUILayout.Button("Hide Debug", GUILayout.Height(Screen.height * 0.2f))) showDebug = false;
                GUILayout.EndHorizontal();

                try
                {

                    GUILayout.Label($"This is a development build");
                    GUILayout.Label($"Time since scene load: {debug_timeOfLoad - Time.time} secconds");
                    GUILayout.Label($"Current Step Progress: {PlayerPrefs.GetInt(DataPrefix + PlayerPrefVariableName)}.");
                    if (activeStep != null) GUILayout.Label($"Prefab is {(activeStep.prefab.activeSelf ? "active" : "inactive")} and {(activeStep.prefab.scene.name == null ? "not part of a scene" : "part of a scene")}");
                    GUILayout.Label($"User feedback - Look for marker: {(debug_lookForMarker ? "Yes" : "No")}");
                    GUILayout.Label($"Active Prefab:  {(activeStep == null ? "no active step" : activeStep.prefab.name)}");
                    GUILayout.Label($"Checking if step is complete: {doCheckForExperienceCompleted} <- Should only be true when a prefab is active");


                    GUILayout.Label($"== Marker Placement info: ==");
                    GUILayout.Label($"Prefab distance to marker is {markerDistance} and angle diff is {markerAngle}. It has been stable for {debug_timeSpentConverged} secconds");


                    GUILayout.Label($"== Images in the library: ==");
                    GUILayout.Label(debug_imagesInLibrary);

                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Reset Ar session using Reset()"))
                    {

                        try
                        {
                            _ARSession.Reset();

                        }
                        catch (Exception ex)
                        {

                            Unity_Utilities.Toast.Show($"Reset Failed: {ex.Message}");
                        }


                    }
                    if (GUILayout.Button($"{(ImageManager.gameObject.activeSelf ? "Disable" : "Enable")} AR tracked image manager"))
                    {
                        EnableImageFinding(!ImageManager.gameObject.activeSelf);
                    }
                    GUILayout.EndHorizontal();

                    string debugInfo = "";
                    debugInfo += $"Ar Session state: {ARSession.state}";
                    debugInfo += $"\nAr Session requested tracking mode: {_ARSession.requestedTrackingMode}";
                    debugInfo += $"\nAr Session current tracking mode: {_ARSession.currentTrackingMode}";
                    debugInfo += $"\nAr Not tracking reason: {ARSession.notTrackingReason}";
                    debugInfo += $"\nAr Subsystem: {_ARSession.subsystem}";
                    debugInfo += $"\n";
                    debugInfo += $"\nNumber of Trackables: {ImageManager.trackables.count}";

                    if (SessionOrigin != null) debugInfo += $"\nDistance between camera and origin: {Vector3.Distance(SessionOrigin.camera.transform.position, SessionOrigin.transform.position)}";



                    debugInfo += "\n== Image Markers ==";
                    foreach (var trackedImage in ImageManager.trackables)
                    {
                        debugInfo += $"\nTracked Images: {trackedImage.referenceImage.name} at {trackedImage.transform.position}. State: {trackedImage.trackingState}";


                    }

                    GUILayout.Label(debugInfo);
                }
                catch (Exception e)
                {

                    GUILayout.Label($"Debug Failure: {e.Message}");
                }
            }
            else if (GUILayout.Button("Show Debug")) showDebug = true; ;


            GUILayout.EndArea();


        }
#endif
        #endregion


        public override void AR_HandleImagesFound(List<ARTrackedImage> images)
        {

           

            Unity_Utilities.Toast.Show($"New image marker detected!");


            if (doCheckForExperienceCompleted)
            {
                Unity_Utilities.Toast.Show($"Aborting spawn of a step. Step {activeStepGameObject.name} is already playing");
                return;
            }

            OnImageDetected?.Invoke();
            Transform imageMarkerTransform = images[0].transform;

            #region Null Check incomming values
            //Null check - Should be inspossible to happen, as there is a test for this further up anyway
            if (images == null)
            {
                Unity_Utilities.Toast.Show($"The list of new Images does not exist!");
                return;
            }
            if (images.Count < 1)
            {
                Unity_Utilities.Toast.Show($"The list of found markers does not contain any new markers!");
                return;
            }
            Unity_Utilities.Toast.Show($"Name of found trackable: {images[0].referenceImage.name} {(images.Count != 1 ? "However, more than one marker was found!" : "")}");
            #endregion

            #region Match a step to this marker
            //Find the key that coresponds with this marker
            foreach (StepObject s in StepList)
            {
                foreach (ImageMarkerObject itemMarker in s.marker)
                {
                    if (images[0].referenceImage.name == itemMarker.name)
                    {
                        activeStep = s;
                        Unity_Utilities.Toast.Show($"TrackableImage {images[0].referenceImage.name} matches a the step {s.prefab.name}");
                    }
                }
            }

            if (activeStep == null)
            {
                Unity_Utilities.Toast.Show($"The found marker does not have a match in the list of steps. This should never happen");
                return;
            }

            #endregion

            #region Check player progress
            if (GetProgress() < activeStep.stepLevelRequirement)
            {
                if (overrideNotYetMessage) { NotYetMessage.Invoke(); }
                else { Unity_Utilities.Toast.Show($"The requirements for opening this step has not been met. You are at level {GetProgress()} while this requires {activeStep.stepLevelRequirement}"); }
                return;
            }
            else Unity_Utilities.Toast.Show($"Progress is far enough to start, activating prefab {activeStep.prefab.name}");

            #endregion

            #region Spawn and place Step

            //instantiate
            activeStepGameObject = Instantiate(activeStep.prefab);
            Unity_Utilities.Toast.Show($"GameObject {activeStepGameObject.name} has been spawned.");

            //Set position
            StartCoroutine(PlaceAnchor(imageMarkerTransform, activeStepGameObject.transform));

            #endregion

            doCheckForExperienceCompleted = true;

            //TODO: Add code to disable perfomance heavy AR object when an image has been detected
            EnableImageFinding(false);

        }
        private void Update()
        {
            if (doCheckForExperienceCompleted)
            {
                doCheckForExperienceCompleted = false;
                if (!activeStepGameObject.activeSelf)
                {
                    CheckLevelCleanup();
                    StepObjectCleanup();
                    EnableImageFinding(true);
                }


                //TODO: Reenable performance heavy AR code
            }
        }

        private void CheckLevelCleanup()
        {

            if (activeStep == null)
            {
                Unity_Utilities.Toast.Show($"Level increase on step completion cancelled, no active step to complete!");
            }

            //Increment player prefs
            if (GetProgress() == activeStep.stepLevelRequirement)
            {
                if (activeStep.incrementStepProgressOnStepComplete)
                {
                    IncrementStepProgress();
                }
                Unity_Utilities.Toast.Show($"This step is marked as a step that should not increment progress on completion. Progress remains at {GetProgress()}");
            }
            else
            {
                Unity_Utilities.Toast.Show($"Current step ({activeStep.stepLevelRequirement}) does not EXACTLY match current progress ({GetProgress()}). Current step has not been increased");
            }


        }

        public void StepObjectCleanup()
        {
            #region Null Check
            if (activeStepGameObject == null)
            {
                Unity_Utilities.Toast.Show($"Aborting cleanup of step. No step is active!");
                return;
            }
            #endregion

            doCheckForExperienceCompleted = false;

            Unity_Utilities.Toast.Show($"The step {activeStepGameObject.name} have been marked as complete. Now performing cleanup!");

            Destroy(activeStepGameObject);
            activeStep = null;

            Unity_Utilities.Toast.Show($"ActiveStep {activeStepGameObject.name} has been destroyed");
            debug_lookForMarker = true;

        }

        private IEnumerator PlaceAnchor(Transform trackedImageMarker, Transform spawnedObject)
        {
            if (spawnedObject.GetComponent<ARAnchor>() != null)
            {
                Unity_Utilities.Toast.Show($"EHHH, an object that allready had an anchor was placed, this is bad practice. Remove the anchor from the prefab! ");
                Destroy(spawnedObject.GetComponent<ARAnchor>());
            }

            int frames = 0;

            spawnedObject.SetParent(trackedImageMarker.parent); //put object in Trackables group. The anchor does not seem to be updated correctly when not in the "trackables group" aka parent of the other image targets

            float timeSpentConverged = 0;
            while (timeSpentConverged < thresholdTime && trackedImageMarker != null && spawnedObject != null)
            {
                markerDistance = Vector3.Distance(spawnedObject.transform.position, trackedImageMarker.transform.position);
                markerAngle = Quaternion.Angle(spawnedObject.transform.rotation, trackedImageMarker.transform.rotation);

                if (markerDistance < thresholdDistance && markerAngle < thresholdAngle)
                {
                    timeSpentConverged += Time.deltaTime;
                }
                else
                {
                    timeSpentConverged = 0;
                }
                debug_timeSpentConverged = timeSpentConverged;

                if (timeSpentConverged > thresholdTime / 3f) Unity_Utilities.Toast.Show($"Prefab and marker stable for {timeSpentConverged} secconds! 1/3. Divergence is {markerDistance}m and {markerAngle}°");
                if (timeSpentConverged > thresholdTime / 3f * 2f) Unity_Utilities.Toast.Show($"Prefab and marker stable for {timeSpentConverged} secconds! 2/3. Divergence is {markerDistance}m and {markerAngle}°");

                spawnedObject.transform.SetPositionAndRotation(
                    Vector3.Lerp(spawnedObject.transform.position, trackedImageMarker.transform.position, lerpStrenght),
                    Quaternion.Lerp(spawnedObject.transform.rotation, trackedImageMarker.transform.rotation, lerpStrenght));

                frames++;

                yield return null;

            }
            Unity_Utilities.Toast.Show($"Position of step prefab and tracked image transform has converged. It tok {frames} frames to do so. Object has been anchored ");


            if (spawnedObject.GetComponent<ARAnchor>() == null)
            {
                spawnedObject.gameObject.AddComponent<ARAnchor>();
            }
        }

        private void EnableImageFinding(bool isEnabled)
        {
            if (useOptimizedCode)
            {
                ImageManager.gameObject.SetActive(isEnabled);
                Unity_Utilities.Toast.Show($"ArTrackedImage manager disabled");
            }
        }

        public int GetProgress()
        {
            return PlayerPrefs.GetInt(DataPrefix + PlayerPrefVariableName, 0);
        }


        /// <summary>
        /// For increasing and decreseasing the step progress
        /// </summary>
        public void IncrementStepProgress(int amount = 1)
        {
            if (PlayerPrefs.HasKey(DataPrefix + PlayerPrefVariableName))
            {
                int value = PlayerPrefs.GetInt(DataPrefix + PlayerPrefVariableName);
                value += amount;
                PlayerPrefs.SetInt(DataPrefix + PlayerPrefVariableName, value);
            }
            else
            {
                PlayerPrefs.SetInt(DataPrefix + PlayerPrefVariableName, 0);
            }
            Unity_Utilities.Toast.Show($"{PlayerPrefVariableName} {(amount > 0 ? "increased" : (amount < 0 ? "decreased" : "has not been adjusted"))}. It is now {PlayerPrefs.GetInt(DataPrefix + PlayerPrefVariableName)}");
        }

        /// <summary>
        /// For debug puposes to clear saved progress
        /// </summary>
        public void ResetSavedVariables()
        {
            PlayerPrefs.DeleteKey(DataPrefix + PlayerPrefVariableName);
            Unity_Utilities.Toast.Show($"Variable {PlayerPrefVariableName} deleted");

        }

        private void OnValidate()
        {
            // Textures needs to:
            // Be READ / WRITE enabled!
            // No Compression! ()
            // Maybe set Filter Mode to point / unfiltered? 
            // Do not generate mip maps? 

            foreach (StepObject s in StepList)
            {
                foreach (ImageMarkerObject marker in s.marker)
                {
                    if (!marker.image.isReadable) Debug.LogError($"WARNING!{marker.name} is not readable. Image markers must read/write enabled. Select image and in inspector under advanced, enable read/write");
                }
                //if(!item.image.format)

            }

            if (string.IsNullOrEmpty(DataPrefix)) Unity_Utilities.Toast.Show("WARNING! Complex Ar Loader. Must have a prefix, see tooltip.");
        }

    }
}
