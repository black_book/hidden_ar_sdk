using BlackBook.Hidden.SDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class ARSessionDebugMenu : MonoBehaviour
{


    public ARSession Session;
    public ARTrackedImageManager TrackedImageManager;
    public ARSessionOrigin SessionOrigin;
    public ComplexARLoader loader;
    public Text text;
    

    // Update is called once per frame
    void Update()
    {
        string debugInfo = "";
        debugInfo += $"Ar Session state: {ARSession.state}";
        debugInfo += $"\nAr Session requested tracking mode: {Session.requestedTrackingMode}";
        debugInfo += $"\nAr Session current tracking mode: {Session.currentTrackingMode}";
        debugInfo += $"\nAr Not tracking reason: {ARSession.notTrackingReason}";
        debugInfo += $"\nAr Subsystem: {Session.subsystem}";
        debugInfo += $"\n";
        debugInfo += $"\nNumber of Trackables: {TrackedImageManager.trackables.count}";

        debugInfo += $"\nDistance between camera and origin: {Vector3.Distance(SessionOrigin.camera.transform.position,SessionOrigin.transform.position)}";
        debugInfo += $"\nStep Progress: {loader.GetProgress()}";


        debugInfo += "\n== Image Markers ==";
        foreach (var trackedImage in TrackedImageManager.trackables)
        {
            debugInfo += $"\nTracked Images: {trackedImage.referenceImage.name} at {trackedImage.transform.position}. State: {trackedImage.trackingState}";

        }

        text.text = debugInfo;
    }
}
