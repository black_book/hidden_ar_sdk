using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace BlackBook.Hidden.SDK
{
    [AddComponentMenu("Black Book/Simple AR PlaneTracker")]
    public class SimpleARPlaneTracker : MonoBehaviour
    {
        public UnityEvent onPlaneFound;
        public UnityEvent onPlanePressed;

        public bool ShowTargetIndicatorAfterPlacement;

        [Header("Optional")]
        [Tooltip("Can be a prefab or an object in the scene. If it is a scene object, it will become deactivated on scene load")]
        public GameObject objectToActivate;
        [Tooltip("Search the project for a prefab named \"TargetIndicator\"")]
        public GameObject targetIndicator;
        
        private Vector3 screenCenter;        
        private Pose PlacementPose;
        private bool placementPoseIsValid = false;
        private bool placementPlaneWasFound;
        private bool objectWasPlaced;
        ARRaycastManager raycastManager;

        void Start()
        {
            placementPlaneWasFound = false;

            //get center of screen
            if (Camera.current != null)
                screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            else
                screenCenter = new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0);

            //get reference to raycast
            raycastManager = FindObjectOfType<ARRaycastManager>();
            if (raycastManager == null)
                BlackBook.Unity_Utilities.Toast.Show("Error, this scene needs an ARRaycastManager, but none was found!!");


            //ensure that targetIndicator is not a prefab
            if (targetIndicator != null)
            {
                if (targetIndicator.scene == null || targetIndicator.scene.name == null) { targetIndicator = Instantiate(targetIndicator); }
                targetIndicator.SetActive(false);
            }

            //ensure that objectToActivate is not a prefab
            if (objectToActivate != null)
            {
                if (objectToActivate.scene == null || objectToActivate.scene.name == null) { objectToActivate = Instantiate(objectToActivate); }
                objectToActivate.SetActive(false);
            }
        }
        void Update()
        {
            if(raycastManager == null)
                return;

            //Updates ground plane
            UpdatePlacementPose();

            //Updates the indicator if the object has not been placed, or ShowTargetIndicatorAfterPlacement is true
            UpdateTargetIndicator();

            //Checks for input and places (or moves) object
            PlacePrefabOnTouch();
        }

        private void UpdatePlacementPose()
        {
            var hits = new List<ARRaycastHit>();
            if(raycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinPolygon))
            {
                placementPoseIsValid = hits.Count > 0;

                if (placementPoseIsValid)
                {
                    PlacementPose = hits[0].pose;

                    if (!placementPlaneWasFound)
                    {
                        placementPlaneWasFound = true;
                        onPlaneFound?.Invoke();
                    }
                }
            }
            else
            {
                placementPoseIsValid = false;
            }          
        }
        
        private void UpdateTargetIndicator()
        {
            if (targetIndicator != null)
            {
                if (!objectWasPlaced || ShowTargetIndicatorAfterPlacement)
                {
                    targetIndicator.SetActive(placementPoseIsValid);

                    if (placementPoseIsValid != null)
                        targetIndicator.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
                }
                else
                {
                    targetIndicator.SetActive(false);
                }
            }           
        }
        
        void PlacePrefabOnTouch()
        {
            if (!placementPoseIsValid || Input.touchCount <= 0)
                return;

            var touch = Input.GetTouch(0);
            if (touch.phase != TouchPhase.Ended)
                return;

            if (!objectWasPlaced) //If the object has not been placed
            {
                objectWasPlaced = true;

                if (objectToActivate != null)
                {
                    objectToActivate.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
                    objectToActivate.SetActive(true);
                    ICareAboutTracking.ARPlacedObjectAdded?.Invoke(objectToActivate);
                }

                //Call placement event
                onPlanePressed?.Invoke();                
            }
            else //update position if it does. //TODO clean up this, accidental touches WILL move the creature even if that was not the users intention
            {
                if (objectToActivate != null)
                {
                    objectToActivate.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
                    ICareAboutTracking.ARPlacedObjectMoved?.Invoke(objectToActivate);
                }                    
            }
        }

        //TODO check if tracking is lost, and call:
        //ICareAboutTracking.ARPlacedObjectLost(spawnedObject);

    }//monobehavior end
}//namespace end