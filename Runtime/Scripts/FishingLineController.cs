﻿using System.Collections.Generic;
using UnityEngine;

public class FishingLineController : MonoBehaviour
{
    public Transform[] lineGuides; // Transforms representing the line guides on the rod
    public LineRenderer lineRenderer; // LineRenderer component to draw the line
    public float lineResolution = 10; // Resolution for each parabola segment
    public float parabolaHeight = 2f; // Control the parabola height between guides
    public float finalParabolaHeight = 5f; // Height of the last parabola segment (end of rod to the hook)

    private List<Vector3> linePositions = new List<Vector3>(); // Stores the positions for the line renderer

    void Start()
    {
        if (lineRenderer == null)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
    }

    void Update()
    {
        UpdateLinePositions();
        RenderLine();
    }

    private void UpdateLinePositions()
    {
        linePositions.Clear();

        for (int i = 0; i < lineGuides.Length - 1; i++)
        {
            Vector3 startPoint = lineGuides[i].position;
            Vector3 endPoint = lineGuides[i + 1].position;
            float currentParabolaHeight = (i == lineGuides.Length - 2) ? finalParabolaHeight : parabolaHeight;

            for (float ratio = 0; ratio <= 1; ratio += 1 / lineResolution)
            {
                Vector3 parabolaPoint = SampleParabola(startPoint, endPoint, currentParabolaHeight, ratio);
                linePositions.Add(parabolaPoint);
            }
        }

        // Add the end point of the last segment
        linePositions.Add(lineGuides[lineGuides.Length - 1].position);
    }

    private void RenderLine()
    {
        lineRenderer.positionCount = linePositions.Count;
        lineRenderer.SetPositions(linePositions.ToArray());
    }

    Vector3 SampleParabola(Vector3 start, Vector3 end, float height, float t)
    {
        Vector3 travelDirection = end - start;
        Vector3 levelDirection = end - new Vector3(start.x, end.y, start.z);
        Vector3 right = Vector3.Cross(travelDirection, levelDirection);
        Vector3 up = Vector3.Cross(right, levelDirection);
        if (end.y > start.y) up = -up;
        Vector3 result = start + t * travelDirection;
        result += ((Mathf.Sin(t * Mathf.PI) * height) * up.normalized);
        return result;
    }
}