﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackBook.Hidden.SDK
{
    [AddComponentMenu("Black Book/Timer Event")]
    public class TimerEvent : MonoBehaviour
    {
        //TODO add help box with:
        //Timers can be run as a component, running on start
        //Or by calling static method Run
        //in both instances, OnUpdate is called every frame returning the progress as 0-1 value
        //and OnComplete is called at the end of the timer
        public float Duration;
        public UnityEvent<float> OnUpdate;
        public UnityEvent OnComplete;

        /// <summary>
        /// If this is attached as a component, it runs automatically on Start
        /// </summary>
        void Start()
        {
            Run(this, Duration, OnUpdate, OnComplete);
        }

        /// <summary>
        /// Runs a new timer
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="duration"></param>
        /// <param name="OnUpdate"></param>
        /// <param name="OnComplete"></param>
        public static void Run(MonoBehaviour owner, float duration, UnityEvent<float> OnUpdate = null, UnityEvent OnComplete = null)
        {
            owner.StartCoroutine(Running(duration, OnUpdate, OnComplete));
        }

        private static IEnumerator Running(float duration, UnityEvent<float> OnUpdate, UnityEvent OnComplete)
        {
            float t = 0;
            while (t <= duration)
            {
                t += Time.deltaTime;
                OnUpdate?.Invoke(t / duration);

                yield return null;
            }

            OnComplete?.Invoke();
        }
    }
}
