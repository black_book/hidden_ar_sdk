using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BlackBook.Hidden.SDK
{
    /// <summary>
    /// Helper class for storing image target information
    /// </summary>
    [CreateAssetMenu(fileName = "New Image Marker", menuName = "ImageMarker/New Image Marker Object")]
    public class ImageMarkerObject : ScriptableObject
    {
        public string name;
        public Texture2D image;
        public float sizeInMeters;

    }
}