using BlackBook.Unity_Utilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using System.Collections;
using static BlackBook.Hidden.SDK.ArRuntimeLibraryBuilder;

/// <summary>
/// Listens to AR foundation and spawns a prefab on a tracked image what it is detected.
/// </summary>

/// <summary>
/// added a bunch of "yield return new WaitForSecondsRealtime(0.1f);" to the download process, in hopes of "ensuring" that it is complete
/// - Thomas Beswick, 19/07/2022 
/// </summary>


namespace BlackBook.Hidden.SDK
{
    //will automatically add Image and Anchor managers if they aren't already present
    [RequireComponent(typeof(ARTrackedImageManager))]
    [RequireComponent(typeof(ARAnchorManager))]
    [AddComponentMenu("Black Book/Simple AR ImageTracker")]
    public class SimpleARImageTracker : ICareAboutTracking
    {
        //TODO Add tooltips so that it easier for content creator and developer to use the scripts correctly
        [Tooltip("This object will be spawned on the origo of the marker, using the same orientation.")]
        public GameObject targetObject;
        [Tooltip("The images you want to detect. Will be added to a Reference Image Library at runtime.")]
        public ImageMarkerObject[] imageMarkers;
        [Tooltip("If false the object is parented to the Image Marker and will constantly update its position based on the image." +
                "\n If true it will just spawn where the Image Marker is found and then be tracked independently using Anchors. " +
                "Useful if you don't want to have the object move with the image, or to reduce jittering if it's offset far from the image marker")]
        public bool trackSeparatelyFromImage = false;

        [Tooltip("Callback for bolt and other behviour, if needed")]
        public UnityEvent OnMarkerFound;

        private void OnValidate()
        {
            // Textures needs to:
            // Be READ / WRITE enabled
            // Turn off Compression (or use a specific format we don't know?)
            // Maybe set Filter Mode to point / unfiltered? 
            // Do not generate mip maps? 
            foreach (var item in imageMarkers)
            {
                if (!item.image.isReadable) Toast.Show("WARNING! Image markers must read/write enabled. Select image and in inspector under advanced, enable read/write");
                //if(!item.image.format)

            }
        }

        //Start happens after Awake
        private void Start()
        {
            StartCoroutine(DoLoad());
        }

        IEnumerator DoLoad()
        {
            yield return new WaitForSecondsRealtime(0.1f);

            if(imageMarkers.Length == 0)
            {
                Toast.Show($"ImageMarkers list in scene is empty.");
            } else
            {
                //Build the library      
                BuildNewLibrary(ImageManager, imageMarkers, () =>
                {
                    Toast.Show($"Library is complete and ready for use. It has {ImageManager.referenceLibrary.count} / {imageMarkers.Length} image markers");
                    // From what i understand if the manager has no library, it will disable itself until it has because the library is not allowed to be null. 
                    // Therefore we must enable manager when done in case it got or was disabled.
                    ImageManager.enabled = true;
                });

                if (targetObject != null)
                {
                    if (targetObject.scene.name != null) //if the object is in the scene (Not a prefab) deactivate it until the image is detected.
                    {
                        targetObject.SetActive(false);
                    }
                }
            }
        }

        public override void AR_HandleImageFound(ARTrackedImage image)
        {
            Unity_Utilities.Toast.Show($"New image marker detected! It is {image.referenceImage.name}");
            OnMarkerFound?.Invoke();

            if (targetObject != null) //Should never be null btw
            {
                //if the gameobject doesn't exist in the scene, meaning it's just a prefab in the project directory, then instantiate it.
                if (targetObject.scene == null || targetObject.scene.name == null)
                {
                    if (trackSeparatelyFromImage)
                    {
                        //spawn the object as a sibling of the image target and set its transform to match.
                        GameObject spawnedObject = Instantiate(targetObject, image.transform.parent);
                        spawnedObject.transform.localPosition += image.transform.localPosition;
                        spawnedObject.transform.localRotation = image.transform.localRotation; //not gonna mess with additive rotation yet...
                                                                                               //add an anchor component if the prefab doesn't have one
                        if (spawnedObject.GetComponent<ARAnchor>() == null)
                        {
                            spawnedObject.AddComponent<ARAnchor>();
                        }
                    }
                    else Instantiate(targetObject, image.transform);
                }
                //if it is in the scene simply reactivate it and parent it under the image marker
                else
                {
                    targetObject.SetActive(true);

                    if (trackSeparatelyFromImage)
                    {
                        //parent object in the Trackables group, as a sibling of the Image Marker, and match its transforms.
                        targetObject.transform.parent = image.transform.parent;
                        targetObject.transform.localPosition += image.transform.localPosition;
                        targetObject.transform.localRotation = image.transform.localRotation;

                        //add an anchor component if the prefab doesn't have one
                        if (targetObject.GetComponent<ARAnchor>() == null)
                        {
                            targetObject.AddComponent<ARAnchor>();
                        }
                    }
                    else
                    {
                        targetObject.transform.SetPositionAndRotation(image.transform.position, image.transform.rotation);
                        targetObject.transform.SetParent(image.transform);
                    }
                }
            }
            else
            {
                Toast.Show("Marker found, but no creature assigned by creator when the scene was created. Assign a prefab to prefab To Spawn! Creating a dummy object", false);
                GameObject dummy = SpawnDummy(image.referenceImage.name);
                dummy.transform.SetPositionAndRotation(image.transform.position, image.transform.rotation);
                dummy.transform.SetParent(image.transform);
            }
        }

        private GameObject SpawnDummy(string name)
        {
            GameObject g = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            g.GetComponent<Renderer>().material.color = Color.cyan;
            //g.transform.position = image.transform.position;
            //g.transform.rotation = image.transform.rotation;

            GameObject text = Instantiate(new GameObject());
            text.transform.SetParent(g.transform);
            text.transform.localPosition = new Vector3(0, .5f, 0);
            TextMesh tMesh = text.AddComponent<TextMesh>();

            tMesh.text = name;

            tMesh.alignment = TextAlignment.Center;
            tMesh.anchor = TextAnchor.LowerCenter;
            tMesh.characterSize = .3f;

            return g;
        }

    }//monobehavior end
}//namespace en