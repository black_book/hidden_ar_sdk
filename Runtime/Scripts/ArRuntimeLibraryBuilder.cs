using BlackBook.Unity_Utilities;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Events;

//Mattias Tronslien
//For image tracking to work with streaming we need to build the libraries runtime. EVERY. TIME.

namespace BlackBook.Hidden.SDK
{
    // https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@4.1/manual/tracked-image-manager.html
    public class ArRuntimeLibraryBuilder
    {



        /// <summary>
        /// Builds a Mutable Runtime Library with the markers during runtime. This requires some computation on the device and can get expensive.
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="images"></param>
        /// <param name="onComplete"></param>
        public static void BuildNewLibrary(ARTrackedImageManager manager, ImageMarkerObject[] images, UnityAction onComplete)
        {
            manager.StartCoroutine(PrivateBuildNewLibrary(manager, images, onComplete));
        }
        /// <summary>
        /// Builds the a library for the manager being referenced, if the plattform does not support mutable runtime libraries, it returns null.
        /// Handles some error and debug as well.
        /// </summary>
        private static IEnumerator PrivateBuildNewLibrary(ARTrackedImageManager manager, ImageMarkerObject[] images, UnityAction onComplete)
        {
            // Toast.Show($"{manager}");
            Toast.Show("======= PrivateBuildNewLibrary =======");

            //CRASHES HERE AGAIN
            try
            {
              //If you craete a runtimer library more than once during the same frame, it will crash the application
                manager.referenceLibrary = manager.CreateRuntimeLibrary(); //Create an empty runtime library, according to docs /

            }
            catch (Exception e)
            {
                Toast.Show(e.Message);
            }

            yield return null;

            try
            {
                //Success
                DoesSupportMutableImageLibraries(manager);
                Toast.Show("Attempt to create library and test for submodules was a success!");
            }
            catch (Exception e)
            {

                //If the plattform does not suport mutable runtime libraries, this code will not work anyway so it breaks out and throws an error
                Toast.Show($"{e.Message}", false);
                Toast.Show($"The current plattform does not support image tracking", false);
                yield break;
            }

            //Toast.Show($"The current plattform {(DoesSupportMutableImageLibraries(manager) ? "does" : "does not")} support mutable runtime libraries"); //Confirmed to work on Android       

            //Add images and callback when done
            int complete = 0;
            
            foreach (ImageMarkerObject image in images)
            {
                manager.StartCoroutine(AddReferenceImage(image, manager, () =>
                {
                    //When the add reference image is done, increment the complete variable above
                    complete++;
                }));
            }
            while (complete < images.Length)
            {
                yield return null;
            }
            //Toast.Show("Library Complete");
           
            onComplete?.Invoke();

        }


        private static IEnumerator AddReferenceImage(ImageMarkerObject imageToAdd, ARTrackedImageManager managerToAddTo, UnityAction callback)
        {
            //Toast.Show($"reference library is of type {man.referenceLibrary.GetType()} "); //turns out it is of type "AR Core ImageDatabase"
            if (managerToAddTo.referenceLibrary is MutableRuntimeReferenceImageLibrary mutableLibrary) //Casts and assigns, from the docs
            {
                //Toast.Show($"Trying to add {imageToAdd.name} to library");
                if (imageToAdd == null || imageToAdd.image == null) Toast.Show($"The image you are trying to add is NULL");
                AddReferenceImageJobState jobState;
                try
                {
                    jobState = mutableLibrary.ScheduleAddImageWithValidationJob(imageToAdd.image, imageToAdd.name, imageToAdd.sizeInMeters);
                }
                catch (Exception e)
                {
                    Toast.Show(e.Message);
                    Toast.Show("The image is not the correct format. Can't add to library.");
                    // Textures needs to:
                    // Be READ / WRITE enabled
                    // Turn off Compression (or use a specific format we don't know?)
                    // Maybe set Filter Mode to point / unfiltered? 
                    // Do not generate mip maps? 
                    throw e;
                }
                //Toast.Show("Image Job running...");

                while (!jobState.jobHandle.IsCompleted)
                {
                    yield return null;
                }
                //Jobb takes about 0.02 secconds per image
            }
            yield return null;
            callback?.Invoke();
        }



        // Update is called once per frame
        void Update()
        {

        }

        private static bool DoesSupportMutableImageLibraries(ARTrackedImageManager man)
        {
            return man.descriptor.supportsMutableLibrary;
        }


    }
}