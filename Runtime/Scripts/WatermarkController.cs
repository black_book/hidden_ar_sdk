﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using BlackBook.Hidden.SDK;

namespace BlackBook.Hidden.Core.ARUX
{
    [AddComponentMenu("Black Book/Watermark Controller")]
    public class WatermarkController : MonoBehaviour
    {
        public CanvasGroup Watermark; public float AnimDuration; private Image watermarkImage; public Image WatermarkImage
        {
            get
            {
                if (watermarkImage == null)
                    watermarkImage = Watermark.GetComponent<Image>();
                return watermarkImage;
            }
        }
        public void ShowWatermark(bool isVisible)
        {
            StopAllCoroutines();
            if (isVisible)
            {
                if (WatermarkImage == null)
                    return;
                StartCoroutine(DoAnimate(1));
            }
            else
            {
                StartCoroutine(DoAnimate(0));
            }
        }
        public void SetWatermark(Sprite sprite)
        {
            if (sprite != null)
            {
                WatermarkImage.sprite = sprite;
            }
        }
        private IEnumerator DoAnimate(float targetValue)
        {
            float wasValue = Watermark.alpha;
            float t = 0;
            while (t <= AnimDuration)
            {
                t += Time.deltaTime;
                float l = t / AnimDuration;
                Watermark.alpha = Mathf.Lerp(wasValue, targetValue, l);
                yield return null;
            }
        }
    }
}
