using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace Assets.Submodules.SDK.Editor.Scripts
{
    public sealed class EventZone
    {
        public EventZone(Image eventZoneImage)
        {
            this.EventZoneImage = eventZoneImage;
        }
        public Image EventZoneImage;
        public bool IsOverlapping = false;
        public DateTime? lastEntered = null;
        public DateTime? lastExited = null;
    }
}