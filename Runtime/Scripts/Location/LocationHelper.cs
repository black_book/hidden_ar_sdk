using Assets.Submodules.SDK.Editor.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Bolt;
using System.Linq;
using System;
using UnityEngine.Android;

public class LocationHelper : MonoBehaviour
{
    private enum PlayerInWorld
    {
        PlayerWithinBounds,
        PlayerOutsideBounds
    }

    private PlayerInWorld? playerInWorld;

    [Tooltip("Image of the map image that is being used as target when translating players geolocation.")]
    public Image MapImage;

    [Tooltip("Image of the marker that should be displayed at the players location on the map.")]
    public Image MarkerImage;

    [Tooltip("List of images that should trigger an event when the markerimage overlaps.")]
    public Image[] EventZones;

    //internal processing with addidtional properties
    private EventZone[] EventZonesProcessing;

    [Tooltip("Minimum latitude for the Game World.")]
    public double MinLatitude;

    [Tooltip("Maximum latitude for the Game World.")]
    public double MaxLatitude;

    [Tooltip("Minimum longitude for the Game World.")]
    public double MinLongitude;

    [Tooltip("Maximum longitude for the Game World.")]
    public double MaxLongitude;

    [Tooltip("Amount in seconds for the frequency of updating the location marker.")]
    public float locationUpdateTime = 2.0f;

    [Tooltip("The service accuracy you want to use, in meters. This determines the accuracy of the device's last location coordinates. Higher values like 500 don't require the device to use its GPS chip and thus save battery power. Lower values like 5-10 provide the best accuracy but require the GPS chip and thus use more battery power. The default value is 10 meters.")]
    public float LocationService_desiredAccuracyInMeters = 1;

    [Tooltip("The minimum distance, in meters, that the device must move laterally before Unity updates Input.location. Higher values like 500 produce fewer updates and are less resource intensive to process. The default is 10 meters.\r\n")]
    public float LocationService_updateDistanceInMeters = 5;

    [Tooltip("Check if you want to log all location updates. Should be disabled on live builds.")]
    public bool LogVerboseLocationUpdates;

    public GeoLocation UserGeoLocation;

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogFormat("LocationHelper initializing..");

        if (MapImage == null || MarkerImage == null)
        {
            if (MapImage == null)
                Debug.LogWarning("LocationHelper: MapImage not defined - can not execute script.");
            else if (MarkerImage == null)
                Debug.LogWarning("LocationHelper: MarkerImage not defined - can not execute script.");
        }
        else
        {
            if (EventZones?.Length > 0)
            {
                EventZonesProcessing = EventZones.Select(ez => new EventZone(ez)).ToArray();
                if (LogVerboseLocationUpdates)
                    Debug.LogFormat($"{EventZonesProcessing.Length} EventZones Loaded.");
            }
            else if (LogVerboseLocationUpdates)
                Debug.LogFormat("No EventZones Loaded.");

            TryInitializeLocationService();
            this.UserGeoLocation = new GeoLocation(0, 0);
        }
    }

    public void TryInitializeLocationService()
    {
        if (UnityEngine.Input.location.status == LocationServiceStatus.Running || UnityEngine.Input.location.status == LocationServiceStatus.Initializing)
            return;

#if UNITY_ANDROID
        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.FineLocation))
        {
            var callbacks = new PermissionCallbacks();
            callbacks.PermissionGranted += PermissionCallbacks_PermissionGranted;
            callbacks.PermissionDenied += PermissionCallbacks_PermissionDenied;
            callbacks.PermissionDeniedAndDontAskAgain += PermissionCallbacks_PermissionDeniedAndDontAskAgain;

            Debug.LogFormat("Android device without access to GPS access detected. Requesting access.");
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.FineLocation, callbacks);
            return;
        }
        else
            Debug.LogFormat("Android device with existing GPS access detected.");

#elif UNITY_IOS
            if (!UnityEngine.Input.location.isEnabledByUser)
            {
                Debug.LogFormat("IOS device detected. No permission to GPS.");
                return;
            }
            else
                Debug.LogFormat("IOS device detected with GPS permission granted.");

#else
            Debug.LogFormat("Desktop device without GPS detected.");

#endif

        Debug.LogFormat("Starting Unity locationservice.");
        Input.location.Start(LocationService_desiredAccuracyInMeters, LocationService_updateDistanceInMeters);
        StartCoroutine("UpdateMarker");
    }

    private void PermissionCallbacks_PermissionGranted(string permissionName)
    {
        Debug.LogFormat("Android GPS permission granted.");
        TryInitializeLocationService();
    }

    private void PermissionCallbacks_PermissionDenied(string permissionName)
    {
        Debug.LogFormat("Android GPS permission denied.");
        CustomEvent.Trigger(this.gameObject, "OnEventGPSPermissionDenied");
    }

    private void PermissionCallbacks_PermissionDeniedAndDontAskAgain(string permissionName)
    {
        Debug.LogFormat("Android GPS permission denied with option don't ask again.");
        CustomEvent.Trigger(this.gameObject, "OnEventGPSPermissionDeniedDontAskAgain");
    }

    IEnumerator UpdateMarker()
    {
        while (true)
        {
            if (LogVerboseLocationUpdates)
                Debug.LogFormat("Trying to update location marker...");

            if (UnityEngine.Input.location.status != LocationServiceStatus.Running)
            {
                if (LogVerboseLocationUpdates)
                    Debug.LogFormat($"LocationService not running yet, current status of service: {Input.location.status}");

                yield return new WaitForSeconds(10);
            }

            var currentLocation = new GeoLocation(Input.location.lastData.latitude, Input.location.lastData.longitude);

            if (LogVerboseLocationUpdates)
                Debug.LogFormat($"Updating location. Lat: {currentLocation.Latitude}, Lon: {currentLocation.Longitude}");

            if (currentLocation.Latitude > MaxLatitude || currentLocation.Latitude < MinLatitude
                || currentLocation.Longitude > MaxLongitude || currentLocation.Longitude < MinLongitude)
            {
                if (LogVerboseLocationUpdates)
                    Debug.LogFormat("Player is outside Game World.");

                if (playerInWorld == null)
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerStartedOutsideWorld");
                else if (playerInWorld == PlayerInWorld.PlayerWithinBounds)
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerExitedWorld");

                playerInWorld = PlayerInWorld.PlayerOutsideBounds;

                HideMarker();
            }
            else if (currentLocation.Latitude == UserGeoLocation.Latitude && currentLocation.Longitude == UserGeoLocation.Longitude)
            {
                if (LogVerboseLocationUpdates)
                    Debug.LogFormat("Player has not moved since last update.");

                //player hasn't moved
                CustomEvent.Trigger(this.gameObject, "OnEventPlayerHasNotMoved");
            }
            else
            {
                if (LogVerboseLocationUpdates)
                    Debug.LogFormat("Players geolocation has changed.");

                UserGeoLocation = currentLocation;
                UpdateMapMarker();
                ShowMarker();
                CheckForEventZoneOverlap();

                if (playerInWorld == null)
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerStartedWithinWorld");
                else if (playerInWorld == PlayerInWorld.PlayerOutsideBounds)
                {
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerEnteredWorld");
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerHasMoved");
                }
                else
                    CustomEvent.Trigger(this.gameObject, "OnEventPlayerHasMoved");

                playerInWorld = PlayerInWorld.PlayerWithinBounds;
            }

            yield return new WaitForSeconds(locationUpdateTime);
        }
    }

    private void ShowMarker()
    {
        MarkerImage.enabled = true;
    }

    private void HideMarker()
    {
        MarkerImage.enabled = false;
    }

    public void UpdateMapMarker()
    {
        var normalizedLongitude = (float)((UserGeoLocation.Longitude - MinLongitude) / (MaxLongitude - MinLongitude));
        var normalizedLatitude = (float)((UserGeoLocation.Latitude - MinLatitude) / (MaxLatitude - MinLatitude));

        if (LogVerboseLocationUpdates)
            Debug.LogFormat($"Calculating normalized values - lat: {normalizedLatitude}, lon: {normalizedLongitude}");

        float translatedX = normalizedLongitude * 100 - 50;
        float translatedY = normalizedLatitude * 100 - 50;
        
        if (LogVerboseLocationUpdates)
        {
            Debug.LogFormat($"Updating location of marker...");
            Debug.LogFormat($"FROM x: {MarkerImage.rectTransform.anchoredPosition.x}, y: {MarkerImage.rectTransform.anchoredPosition.y}");
            Debug.LogFormat($"TO x: {translatedX}, y: {translatedY}");
        }

        MarkerImage.rectTransform.anchoredPosition = new Vector2(translatedX, translatedY);
    }

   
    private void CheckForEventZoneOverlap()
    {
        if (EventZonesProcessing?.Length > 0)
            foreach (var eventZone in EventZonesProcessing)
            {
                if (eventZone.EventZoneImage.gameObject.activeInHierarchy && MarkerImage.rectTransform.rect.Overlaps(eventZone.EventZoneImage.rectTransform.rect))
                {   //player is inside an eventzone

                    if (eventZone.IsOverlapping)
                    {   //player has already been inside this zone.
                        break;
                    }

                    if (LogVerboseLocationUpdates)
                        Debug.LogFormat($"Player position started overlapping with EventZone {eventZone.EventZoneImage.name}");

                    eventZone.IsOverlapping = true;
                    eventZone.lastEntered = DateTime.Now;

                    CustomEvent.Trigger(this.gameObject, "OnEventZoneOverlapStarted", eventZone);
                    break;
                }
                else if (eventZone.IsOverlapping)
                {
                    if (LogVerboseLocationUpdates)
                        Debug.LogFormat($"Player position ended overlapping with EventZone {eventZone.EventZoneImage.name}");

                    eventZone.IsOverlapping = false;
                    eventZone.lastEntered = DateTime.Now;

                    CustomEvent.Trigger(this.gameObject, "OnEventZoneOverlapEnded", eventZone);
                    break;
                }
            }
    }

    // Update is called once per frame
    void Update()
    {

    }
}