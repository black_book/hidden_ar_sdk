using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SimplePostProcessing  : MonoBehaviour
{
    public Material PostProcessingMaterial;

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (PostProcessingMaterial != null)
        {
            Graphics.Blit(src, dst, PostProcessingMaterial);
        }
    }
}
