using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrainCheck {

	public enum LocationManagerOptions 
	{
	  CheckLocationPermission,
	  OpenSettingsForLocationPermissions,
	  OpenGoogleMapsWithCurrentLocation,
	  OpenGoogleMapsInDriveMode,
	  StartLocationMonitoring,
	  StopLocationMonitoring,
	  GetCurrentLatLong,
	  RequestLocationPermission
	}

	public class DemoScript : MonoBehaviour {
		public LocationManagerOptions myOption;
		string gameObjectName = "UnityReceiveMessage";
		string statusMethodName = "CallbackMethod";
		string destinationLat = "28.7041";
		string destinationLong = "77.1025";

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		void OnGUI()
		{
			
		}

		void OnMouseUp() {
	    	StartCoroutine(BtnAnimation());
	 	}

	 	private IEnumerator BtnAnimation()
	    {
	    	Vector3 originalScale = gameObject.transform.localScale;
	        gameObject.transform.localScale = 0.9f * gameObject.transform.localScale;
	        yield return new WaitForSeconds(0.2f);
	        gameObject.transform.localScale = originalScale;
	        ButtonAction();
	    }

	    private void ButtonAction() {
			switch(myOption) 
			{
			    case LocationManagerOptions.CheckLocationPermission:
			      BrainCheck.LocationManagerBridge.SetUnityGameObjectNameAndMethodName(gameObjectName, statusMethodName);
				  BrainCheck.LocationManagerBridge.GetLocationAuthorization();
			      break;
			    case LocationManagerOptions.OpenSettingsForLocationPermissions:
			      BrainCheck.LocationManagerBridge.SetUnityGameObjectNameAndMethodName(gameObjectName, statusMethodName);
				  BrainCheck.LocationManagerBridge.OpenSettingsForLocationPermission();		      
			      break;
			    case LocationManagerOptions.OpenGoogleMapsWithCurrentLocation:
			      BrainCheck.LocationManagerBridge.OpenGoogleMapWithCurrentLocation();
			      break;
			    case LocationManagerOptions.OpenGoogleMapsInDriveMode:
			      BrainCheck.LocationManagerBridge.OpenGoogleMapWithCustomLocation(destinationLat, destinationLong);
			      break;
			    case LocationManagerOptions.StartLocationMonitoring:
			      BrainCheck.LocationManagerBridge.SetUnityGameObjectNameAndMethodName(gameObjectName, statusMethodName);
			      BrainCheck.LocationManagerBridge.StartLocationMonitoring();
			      break;
			    case LocationManagerOptions.StopLocationMonitoring:
			      BrainCheck.LocationManagerBridge.StopLocationMonitoring();
			      break;
			    case LocationManagerOptions.GetCurrentLatLong:
			      BrainCheck.LocationManagerBridge.SetUnityGameObjectNameAndMethodName(gameObjectName, statusMethodName);
			      BrainCheck.LocationManagerBridge.GetCurrentLocation();
			      break;
			    case LocationManagerOptions.RequestLocationPermission:
			      BrainCheck.LocationManagerBridge.SetUnityGameObjectNameAndMethodName(gameObjectName, statusMethodName);
			      BrainCheck.LocationManagerBridge.RequestRunTimeLocationPermission();
			      break;
			}
	    }

	}
}
