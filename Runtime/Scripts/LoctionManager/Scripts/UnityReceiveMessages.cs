using UnityEngine;
using System.Collections;
using System.IO;

namespace BrainCheck {
	public class UnityReceiveMessages : MonoBehaviour {
		public static UnityReceiveMessages Instance;
		private TextMesh tMesh;
		string currentStatus;
		void Awake(){
			Instance = this;
		}

		// Use this for initialization
		void Start () {
			tMesh = gameObject.GetComponent<TextMesh>();
		}

		// Update is called once per frame
		void Update () {
			// tMesh.text = "currentStatus";
		}

		public void CallbackMethod(string currentStatusTemp){
			currentStatus = currentStatusTemp;
			tMesh.text = currentStatus;
		}
	}
}
