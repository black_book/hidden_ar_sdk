﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using BlackBook.Hidden.SDK;

//Mattias Tronslien

namespace BlackBook.Hidden.Core.ARUX
{
    [AddComponentMenu("Black Book/Default UI")]
    public class HiddenDefaultUXBridge : MonoBehaviour
    {
        string UXsceneName = "AR UX Instructions";
        //int UXsceneIndex = 4;
        public UXContentObject data;
        public bool showResetButton = true;

        private void Start()
        {
            StartCoroutine(LoadUX());
        }

        IEnumerator LoadUX()
        {
            //Load Scene
            //TODO: Make this a streamable asset that fetches from server

            // The Application loads the Scene in the background as the current Scene runs.
            // This is particularly good for creating loading screens.
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(UXsceneName, LoadSceneMode.Additive);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            //LoadContent
            UXController UX = FindObjectOfType<UXController>();
            UX.LoadContent(data,showResetButton);
            
        }

    }
}