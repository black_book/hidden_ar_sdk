﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackBook.Hidden.SDK
{
    [AddComponentMenu("Black Book/Dialogue Focus")]
    public class DialogueFocus : MonoBehaviour
    {
        //TODO generalize script
        //make sure relevant variables are exposed to inspector
        //etc.

        //TODO add help box with instructions

        [System.Serializable]
        public class Dialogue
        {
            public Transform dialogCanvas;
            public CanvasGroup interactability;
            public Transform focusTarget;
            public Yarn.Unity.DialogueRunner dialogueRunner;
        }

        public Dialogue[] dialogues;

        // Update is called once per frame
        void Update()
        {
            Dialogue closest = null;
            float distance = 999;

            foreach (Dialogue d in dialogues)
            {
                Vector2 vp = Camera.main.WorldToViewportPoint(d.focusTarget.position);
                float ld = Vector2.Distance(vp, new Vector2(0.5f, 0.5f));
                d.dialogCanvas.localScale = Vector3.one * (1 - ld);
                d.interactability.interactable = false;
                d.interactability.blocksRaycasts = false;

                if (ld < distance && d.dialogueRunner.IsDialogueRunning)
                {
                    distance = ld;
                    closest = d;
                }
            }

            if (closest != null)
            {
                closest.interactability.interactable = true;
                closest.interactability.blocksRaycasts = true;
            }
        }
    }
}
