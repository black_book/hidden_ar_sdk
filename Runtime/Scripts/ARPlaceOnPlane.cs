using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace BlackBook.Hidden.SDK
{
    [AddComponentMenu("Black Book/Place transform on AR Plane")]
    public class ARPlaceOnPlane : MonoBehaviour
    {
        public GameObject activatedChild;
        public Animator animator;
        public string animatorTrigger;

        private ARRaycastManager _raycastManager;
        private ARRaycastManager RaycastManager
        {
            get
            {
                if (_raycastManager == null)
                    _raycastManager = FindObjectOfType<ARRaycastManager>();

                return _raycastManager;
            }
        }

        void OnEnable()
        {
            StartCoroutine(FindGround());
        }

        IEnumerator FindGround()
        {
            var ray = new Ray(transform.position + Vector3.up, Vector3.down);
            var hits = new List<ARRaycastHit>();
            activatedChild.SetActive(false);

            while (true)
            {
                if (RaycastManager.Raycast(ray, hits, TrackableType.PlaneWithinPolygon))
                {
                    if (hits.Count > 0)
                    {
                        transform.SetPositionAndRotation(hits[0].pose.position, hits[0].pose.rotation);
                        activatedChild.SetActive(true);

                        if (animator != null)
                        {
                            animator.SetTrigger(animatorTrigger);
                        }

                        yield break;
                    }
                }

                yield return null;
            }
        }
    }

}
