using UnityEngine.XR.ARFoundation;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

namespace BlackBook.Hidden.SDK {

    public class ICareAboutTracking : MonoBehaviour
    {
        private ARTrackedImageManager imageManager;
        public ARTrackedImageManager ImageManager
        {
            get
            {
                if (imageManager == null) imageManager = FindObjectOfType<ARTrackedImageManager>();                
                return imageManager;
            }
        }

        private ARPlaneManager planeManager;
        public ARPlaneManager PlaneManager
        {
            get
            {
                if (planeManager == null) planeManager = FindObjectOfType<ARPlaneManager>();
                return planeManager;
            }
        }

        void OnEnable()
        {
            if (ImageManager != null) ImageManager.trackedImagesChanged += OnChanged;
            if (PlaneManager != null) PlaneManager.planesChanged += OnChanged;

            ARPlacedObjectAdded += AR_PlacedObjectAdded;
            ARPlacedObjectMoved += AR_PlacedObjectMoved;
            ARPlacedObjectLost += AR_PlacedObjectLost;
        }
        void OnDisable()
        {
            if (ImageManager != null) ImageManager.trackedImagesChanged -= OnChanged;
            if (PlaneManager != null) PlaneManager.planesChanged -= OnChanged;

            ARPlacedObjectAdded -= AR_PlacedObjectAdded;
            ARPlacedObjectMoved -= AR_PlacedObjectMoved;
            ARPlacedObjectLost -= AR_PlacedObjectLost;
        }

        public static UnityAction<GameObject> ARPlacedObjectAdded;
        public static UnityAction<GameObject> ARPlacedObjectMoved;
        public static UnityAction<GameObject> ARPlacedObjectLost;
        

        void OnChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            if (eventArgs.added != null && eventArgs.added.Count > 0)
                AR_HandleImagesFound(eventArgs.added);
            if (eventArgs.updated != null && eventArgs.updated.Count > 0)
                AR_HandleImagesUpdated(eventArgs.updated);
            if (eventArgs.removed != null && eventArgs.removed.Count > 0)
                AR_HandleImagesRemoved(eventArgs.removed);
        }

        void OnChanged(ARPlanesChangedEventArgs eventArgs)
        {
            if (eventArgs.added != null && eventArgs.added.Count > 0)
                AR_PlanesAdded(eventArgs.added);
            if (eventArgs.updated != null && eventArgs.updated.Count > 0)
                AR_PlanesUpdated(eventArgs.updated);
            if (eventArgs.removed != null && eventArgs.removed.Count > 0)
                AR_PlanesLost(eventArgs.removed);
        }

        /// <summary>
        /// This "event" is called when imagetrackers are found
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_HandleImagesFound(List<ARTrackedImage> images)
        {
            foreach (var newImage in images)
            {
                AR_HandleImageFound(newImage);
            }
        }

        /// <summary>
        /// This "event" is called when an imagetracker is found
        /// </summary>
        /// <param name="image"></param>
        public virtual void AR_HandleImageFound(ARTrackedImage image)
        {

        }

        /// <summary>
        /// This "event" is called when imagetrackers are updated
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_HandleImagesUpdated(List<ARTrackedImage> images)
        {
            foreach (var updatedImage in images)
            {
                AR_HandleImageUpdated(updatedImage);
            }
        }

        /// <summary>
        /// This "event" is called when an imagetracker is updated. Updated means that it detects an image that it has previsouly detected.
        /// </summary>
        /// <param name="image"></param>
        public virtual void AR_HandleImageUpdated(ARTrackedImage image)
        {

        }

        /// <summary>
        /// This "event" is called when imagetrackers are removed / lost
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_HandleImagesRemoved(List<ARTrackedImage> images)
        {
            foreach (var removedImage in images)
            {
                AR_HandleImageRemoved(removedImage);
            }
        }

        /// <summary>
        /// This "event" is called when an imagetracker is removed / lost
        /// </summary>
        /// <param name="image"></param>
        public virtual void AR_HandleImageRemoved(ARTrackedImage image)
        {

        }

        /// <summary>
        /// This "event" is called when planes are added
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_PlanesAdded(List<ARPlane> planes)
        {
            foreach (var plane in planes)
            {
                AR_PlaneAdded(plane);
            }
        }

        /// <summary>
        /// This "event" is called when a plane has been added
        /// </summary>
        /// <param name="plane"></param>
        public virtual void AR_PlaneAdded(ARPlane plane)
        {

        }

        /// <summary>
        /// This "event" is called when planes are updated
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_PlanesUpdated(List<ARPlane> planes)
        {
            foreach (var plane in planes)
            {
                AR_PlaneUpdated(plane);
            }
        }

        /// <summary>
        /// This "event" is called when a plane has been updated
        /// </summary>
        /// <param name="plane"></param>
        public virtual void AR_PlaneUpdated(ARPlane plane)
        {

        }

        /// <summary>
        /// This "event" is called when planes are lost
        /// </summary>
        /// <param name="images"></param>
        public virtual void AR_PlanesLost(List<ARPlane> planes)
        {
            foreach (var plane in planes)
            {
                AR_PlaneLost(plane);
            }
        }

        /// <summary>
        /// This "event" is called when a plane has been lost
        /// </summary>
        /// <param name="plane"></param>
        public virtual void AR_PlaneLost(ARPlane plane)
        {

        }

        /// <summary>
        /// This "event" is called when an object is placed on a groundplane
        /// </summary>
        /// <param name="obj"></param>
        public virtual void AR_PlacedObjectAdded(GameObject obj)
        {

        }

        /// <summary>
        /// This "event" is called when a placed object is moved to a new position
        /// </summary>
        /// <param name="obj"></param>
        public virtual void AR_PlacedObjectMoved(GameObject obj)
        {

        }

        /// <summary>
        /// This "event" is called when a placed object's tracking is lost
        /// </summary>
        /// <param name="obj"></param>
        public virtual void AR_PlacedObjectLost(GameObject obj)
        {

        }
    }
}
