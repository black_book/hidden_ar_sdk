﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlackBook.Hidden.SDK
{
    [AddComponentMenu("Black Book/Fade Renderer")]
    public class RendererFade : MonoBehaviour
    {
        public Renderer[] Renderers;
        public string ShaderKey = "_Opacity";

        public void Fade(float to, float duration, UnityEvent onComplete = null)
        {
            StopAllCoroutines();
            StartCoroutine(DoFade(to, duration, onComplete));
        }

        private IEnumerator DoFade(float to, float duration, UnityEvent onComplete)
        {
            float from = Renderers[0].material.GetFloat(ShaderKey);
            float t = 0;

            while (t < duration)
            {
                t += Time.deltaTime;
                SetOpacity(Mathf.Lerp(from, to, t / duration));
                yield return null;
            }

            SetOpacity(to);
        }

        public void SetOpacity(float value)
        {
            foreach (Renderer r in Renderers)
            {
                r.material.SetFloat(ShaderKey, value);
            }
        }
    }
}
