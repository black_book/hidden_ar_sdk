using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowDistanceOverride : MonoBehaviour
{
    public const int Close = 70;
    public const int Default = 150;
    public const int Far = 300;
    public const int UltraFar = 1000;

    public enum Range { Close, Default, Far, UltraFar, Custom }

    [SerializeField] private Range _range;
    [SerializeField] private int custom = 150;

    public Range range
    {
        get
        {
            return _range;
        }
        set
        {
            _range = value;
            Set(value);
        }
    }

    private void Awake()
    {
        Set(custom);
    }

#if UNITY_EDITOR
    private Range e_range;
    private int e_custom;
    private void OnValidate()
    {
        if (e_range != _range)
            Set(_range);
        else if(e_custom != custom)
            Set(custom);

        e_range = _range;
        e_custom = custom;
    }
#endif

    public void Set(Range range)
    {
        switch (range)
        {
            case Range.Close:
                Set(Close);
                break;
            case Range.Default:
                Set(Default);
                break;
            case Range.Far:
                Set(Far);
                break;
            case Range.UltraFar:
                Set(UltraFar);
                break;
        }
    }

    public void Set(int distance)
    {
        custom = distance;
        QualitySettings.shadowDistance = distance;

        if (distance == Close)
            _range = Range.Close;
        else if (distance == Default)
            _range = Range.Default;
        else if (distance == Far)
            _range = Range.Far;
        else if (distance == UltraFar)
            _range = Range.UltraFar;
        else
            _range = Range.Custom;
    }
}
