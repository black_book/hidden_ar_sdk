using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using UnityEngine;
using UnityEngine.Events;
using DeleteObjectRequest = Amazon.S3.Model.DeleteObjectRequest;

namespace Submodules.SDK.Runtime
{
    public class AWSDataAccess : MonoBehaviour
    {
        public const string BUCKET_NAME = "hidden-ar-solution-assetbundles";
        public const string TABLE_NAME = "AR_Content_Lookup-Dev";

        private static AmazonDynamoDBClient dynamoClient;
        private static IAmazonS3 s3Client;

        public enum Error { NoTable, NoRow }

        private static bool hasPinged = false;
        private static bool isFetching = false;

        public void InitializeCredentials(string accessKey, string secretKey)
        {
            AWSCredentials cred = new BasicAWSCredentials(accessKey, secretKey);
            s3Client = new AmazonS3Client(cred, RegionEndpoint.EUCentral1);
            if (dynamoClient != null) return;
            dynamoClient = new AmazonDynamoDBClient(cred, RegionEndpoint.EUCentral1);
            dynamoClient.AfterResponseEvent += AmazonResponse;
            StartCoroutine(PingAmazon());
        }

        #region Amazon Callbacks

        public static IEnumerator PingAmazon()
        {
            while (isFetching) yield return null;
            hasPinged = false;
            Task<ListTablesResponse> getTables = dynamoClient.ListTablesAsync();
            float t = 0;
            while (t < 1)
            {
                t += Time.deltaTime;
                yield return getTables;
            }
            hasPinged = true;
            Debug.Log($"Amazon Ponged with status: {getTables.Status}");
        }

        static void AmazonResponse(object sender, ResponseEventArgs e)
        {
            WebServiceResponseEventArgs args = (WebServiceResponseEventArgs)e;
        }

        #endregion

        #region Check in DynamoDB

        public void CheckForWriteKey(string givenWriteKey, UnityAction<string> onFound, UnityAction<Error> onError)
        {
            StartCoroutine(GetDBAttribute(TABLE_NAME, givenWriteKey, "writeKey", (s) => { onFound?.Invoke(s); }, onError));
        }

        public void CheckForAssetURL(string givenWriteKey, UnityAction<string> onFound, UnityAction<Error> onError)
        {
            StartCoroutine(GetDBAttribute(TABLE_NAME, givenWriteKey, "assetURL", (s) => { onFound?.Invoke(s); }, onError));
        }

        public void CheckForIDSortKey(string givenWriteKey, UnityAction<string> onFound, UnityAction<Error> onError)
        {
            StartCoroutine(GetDBAttribute(TABLE_NAME, givenWriteKey, "id", (s) => { onFound?.Invoke(s); }, onError));
        }

        #endregion

        #region Get Attributes From DynamoDB

        private static IEnumerator GetDBAttribute(string tableName, string givenWriteKey, string attributeToGet, UnityAction<string> onResponse, UnityAction<Error> onError = null)
        {
            ScanFilter filter = new ScanFilter();
            filter.AddCondition("writeKey", ScanOperator.Equal, givenWriteKey);
            yield return GetDBAttribute(tableName, filter, new string[] { attributeToGet }, (s) => { onResponse?.Invoke(s[0]); }, onError);
        }

        private static IEnumerator GetDBAttribute(string tableName, ScanFilter filter, string[] attributeToGet, UnityAction<string[]> onResponse, UnityAction<Error> onError = null)
        {
            yield return GetDBAttribute(tableName, filter, attributeToGet, (s) => { onResponse?.Invoke(s[0]); }, onError);
        }

        private static IEnumerator GetDBAttribute(string tableName, ScanFilter filter, string[] attributeToGet, UnityAction<string[][]> onResponse, UnityAction<Error> onError = null)
        {
            if (!hasPinged)
            {
                while (!hasPinged) yield return null;
            }

            if (isFetching)
            {
                while (isFetching) yield return null;
            }

            isFetching = true;
            ScanOperationConfig config = new ScanOperationConfig
            {
                Filter = filter,
                AttributesToGet = new List<string>(attributeToGet),
                Select = SelectValues.SpecificAttributes
            };

            Task<ListTablesResponse> getTables = dynamoClient.ListTablesAsync();
            yield return getTables;

            if (getTables.Result.TableNames.Contains(tableName))
            {
                Table table = Table.LoadTable(dynamoClient, tableName);
                Search search = table.Scan(config);

                List<List<Document>> docssList = new List<List<Document>>();
                do
                {
                    var getNextBatch = search.GetNextSetAsync();
                    yield return getNextBatch;
                    docssList.Add(getNextBatch.Result);
                } while (!search.IsDone);

                if (docssList.Count == 0)
                    onError?.Invoke(Error.NoTable);
                else
                {
                    if (docssList[0].Count == 0)
                    {
                        onError?.Invoke(Error.NoRow);
                    }
                    else
                    {
                        Document doc = docssList[0][0];

                        string[][] returnList = new string[docssList[0].Count][];
                        for (int d = 0; d < docssList[0].Count; d++)
                        {
                            List<string> values = new List<string>();
                            for (int i = 0; i < attributeToGet.Length; i++)
                            {
                                DynamoDBEntry entry;
                                if (doc.TryGetValue(attributeToGet[i], out entry))
                                {
                                    values.Add(entry.AsString());
                                }
                            }
                            returnList[d] = values.ToArray();
                        }
                        onResponse?.Invoke(returnList);
                    }
                }
            }
            else
            {
                onError?.Invoke(Error.NoTable);
            }

            isFetching = false;
        }

        #endregion

        #region S3 Delete Functions

        public async void DeleteAssetBundlesInSlot(List<string> keys, Action callback)
        {
            if (keys.Count == 0 || keys == null) callback?.Invoke();
            foreach (string key in keys)
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = BUCKET_NAME,
                    Key = key,
                };
                await DeleteOperation(deleteObjectRequest);
            }
            callback?.Invoke();
        }

        private async Task DeleteOperation(DeleteObjectRequest deleteObjectRequest)
        {
            try
            {
                await s3Client.DeleteObjectAsync(deleteObjectRequest);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when deleting an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when deleting an object", e.Message);
            }
        }

        #endregion

        #region S3 Upload Functions

        public static async void UploadAssetBundlesInSlot(List<UploadContentObject> contents, Action callback)
        {
            try
            {
                foreach (UploadContentObject content in contents)
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = BUCKET_NAME,
                        Key = content.KeyName,
                        FilePath = content.FilePath,
                    };
                    await UploadOperation(request);
                }
                callback?.Invoke();
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }

        private static async Task UploadOperation(PutObjectRequest put)
        {
            try
            {
                PutObjectResponse response = await s3Client.PutObjectAsync(put);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when uploading an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when uploading an object", e.Message);
            }
        }

        #endregion

        #region Update DynamoDB

        public async void UpdateURLInDynamoDB(string url, string ownerName, string id, string writeKey, string sceneOrientation, Action callback)
        {
            var updateResponse = await UpdateOperation(url, ownerName, id, sceneOrientation);
            if (updateResponse != null)
            {
                Debug.Log("Update has been completed successfully!");
                callback?.Invoke();
            }
        }

        private async Task<UpdateItemResponse> UpdateOperation(string url, string ownerName, string id, string sceneOrientation)
        {
            try
            {
                var request = new UpdateItemRequest
                {
                    TableName = TABLE_NAME,
                    Key = new Dictionary<string, AttributeValue>
                    {
                        { "id", new AttributeValue { N = id } },
                        { "ownerID", new AttributeValue { S = ownerName } }
                    },
                    ExpressionAttributeNames = new Dictionary<string, string>
                    {
                        { "#P", "assetURL" },
                        { "#V", "assetVersion" },
                        { "#SO", "SceneOrientation" }
                    },
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                    {
                        { ":p", new AttributeValue { S = url } },
                        { ":v", new AttributeValue { N = "1" } },
                        { ":so", new AttributeValue { S = sceneOrientation } }
                    },
                    UpdateExpression = "SET #P = :p, #V = #V + :v, #SO = :so"
                };
                var response = await dynamoClient.UpdateItemAsync(request);
                return response;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion
    }
}
