using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bolt_UnityActionContainer : MonoBehaviour
{
    [System.Serializable]
    public class BoltAction
    {
        public string evt;
        public UnityEvent action;
    }

    public BoltAction[] events;
    
    public void InvokeEvent(string evt)
    {
        foreach(BoltAction ba in events)
        {
            if(ba.evt.Equals(evt))
            {
                ba.action?.Invoke();
                return;
            }
        }
    }
}
