using Bolt;
using Ludiq;
using UnityEngine;

[UnitTitle("Send Toast message")]
[UnitCategory("BlackBook")]
[TypeIcon(typeof(Debug))]
public class Bolt_Toast : Unit
{
    public Bolt_Toast() : base() { }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Message")]
    public ValueInput message { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Enter);
        exit = ControlOutput(nameof(exit));

        message = ValueInput<string>(nameof(message));

        Succession(enter, exit);
    }

    public ControlOutput Enter(Flow flow)
    {
        string msg = flow.GetValue<string>(message);

        BlackBook.Unity_Utilities.Toast.Show(msg);

        return exit;
    }
}
