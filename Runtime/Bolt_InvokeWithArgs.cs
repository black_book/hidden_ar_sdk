using Bolt;
using Ludiq;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[UnitTitle("Invoke UnityAction with Args")]
[UnitCategory("BlackBook")]
[TypeIcon(typeof(UnityAction))]
public class Bolt_InvokeWithArgs : Unit
{
    public Bolt_InvokeWithArgs() : base() { }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlInput enter { get; private set; }

    [DoNotSerialize]
    [PortLabel("Action")]
    public ValueInput action { get; private set; }

    [DoNotSerialize]
    [PortLabel("Value")]
    public ValueInput arg { get; private set; }

    [DoNotSerialize]
    [PortLabelHidden]
    public ControlOutput exit { get; private set; }

    protected override void Definition()
    {
        enter = ControlInput(nameof(enter), Enter);
        exit = ControlOutput(nameof(exit));

        action = ValueInput<UnityAction<GameObject>>(nameof(action));
        arg = ValueInput<GameObject>(nameof(arg));

        Succession(enter, exit);
    }

    public ControlOutput Enter(Flow flow)
    {
        UnityAction<GameObject> act = flow.GetValue<UnityAction<GameObject>>(action);
        GameObject go = flow.GetValue<GameObject>(arg);

        act?.Invoke(go);

        return exit;
    }
}
