using System;
using Tymski;
using UnityEngine;
using Object = UnityEngine.Object;

//Author: Furkan Kurt
//We decided to name this script as "Content Pack" since it must be meaningful to the content creators.

namespace Submodules.SDK.Editor.Scripts
{
    [CreateAssetMenu(fileName = "Content Pack", menuName = "Black Book/New Content Pack", order = 1)]
    [Serializable]
    public class ContentPack : ScriptableObject
    {
        [Header("General Information")]
        [Tooltip("Owner of this Asset Bundle")]
        public string ownersName = "Blackbook";

        [Tooltip("Requires for the authentication of retrieving Data")]
        public string writeKey = "Please enter your key!";

        [Tooltip("If the name of Android and iOS AssetBundle files do not match, this string will be used as the referenced name")]
        public string referenceName = "troll-project";

        [Header("Files")]
        public bool iOS;
        public bool android;

        [Header("Scene")]
        [Tooltip("EXPERIMENTAL FEATURE \nThis will be working for the next version of the asset bundle uploading tool. Select to make asset bundles.")]
        public SceneReference unityScene;

        [Header("Orientation")]
        [Tooltip("Select the desired orientation for the scene")]
        public SceneOrientation sceneOrientation;
    }

    public enum SceneOrientation
    {
        Landscape,
        Portrait,
        Both
    }
}
