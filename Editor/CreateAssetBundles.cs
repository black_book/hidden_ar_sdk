using UnityEditor;
using System.IO;
using Submodules.SDK.Editor.Scripts;
using UnityEngine;

public class CreateAssetBundles
{

    public static string assetBundleDirectory = "Assets/Resources/AssetBundles/";
    private static void MarkBundle(ContentPack contentPack)
    {
#if UNITY_EDITOR
        var importer = UnityEditor.AssetImporter.GetAtPath(contentPack.unityScene.ScenePath);
        importer.assetBundleName = "uploadassetlabel";
#endif
    }
    private static void MarkBundleNull(ContentPack contentPack)
    {
#if UNITY_EDITOR
        var importer = UnityEditor.AssetImporter.GetAtPath(contentPack.unityScene.ScenePath);
        importer.assetBundleName = "";
#endif
    }

    [MenuItem("Assets/Build AssetBundles")]
    public static void BuildAllAssetBundles(ContentPack contentPack)
    {
        MarkBundle(contentPack);
        //if main directory doesnt exist create it
        if (Directory.Exists(assetBundleDirectory)) {
            Directory.Delete(assetBundleDirectory, true);
        }

        Directory.CreateDirectory(assetBundleDirectory);

        if (contentPack.iOS) {
            //create bundles for all platform (use IOS for editor support on MAC but must be on IOS build platform)
            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.iOS);
            AppendPlatformToFileName("IOS");
            Debug.Log("IOS bundle created...");
        }
        if (contentPack.android) {
            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.Android);
            AppendPlatformToFileName("Android");
            Debug.Log("Android bundle created...");
        }

        RemoveSpacesInFileNames();
        AssetDatabase.Refresh();
        Debug.Log("Process complete!");
        MarkBundleNull(contentPack);
    }

    static void RemoveSpacesInFileNames()
    {
        foreach (string path in Directory.GetFiles(assetBundleDirectory)) {
            string oldName = path;
            string newName = path.Replace(' ', '-');
            File.Move(oldName, newName);
        }
    }

    static void AppendPlatformToFileName(string platform)
    {
        foreach (string path in Directory.GetFiles(assetBundleDirectory)) {
            //get filename
            string[] files = path.Split('/');
            string fileName = files[files.Length - 1];

            //delete files we dont need
            if (fileName.Contains(".") || fileName.Contains("Bundle")) {
                File.Delete(path);
            }
            else if (!fileName.Contains("-")) {
                //append platform to filename
                FileInfo info = new FileInfo(path);
                info.MoveTo(path + "-" + platform);
            }
        }
    }
}
