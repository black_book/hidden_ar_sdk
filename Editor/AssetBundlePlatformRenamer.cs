using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AssetBundlePlatformRenamer
{
    [MenuItem("Hidden SDK/Pre Upload/Rename Assets with Build Targets")]
    public static void Renamer()
    {
        //Get correct plattform string format
        string platform = "";
        switch (EditorUserBuildSettings.activeBuildTarget)
        {
            case BuildTarget.StandaloneWindows:
                platform = "StandaloneWindows";
                break;
            case BuildTarget.iOS:
                platform = "iOS";
                break;
            case BuildTarget.Android:
                platform = "Android";
                break;
            case BuildTarget.WSAPlayer:
                platform = "WSAPlayer";
                break;
            case BuildTarget.StandaloneWindows64:
            case BuildTarget.WebGL:
            case BuildTarget.NoTarget:
            default:
                EditorUtility.DisplayDialog("Asset Bundle Renamer", "Current build target is not supported", "Ok");
                break;
        }

        string path = Application.dataPath.Replace("Assets", "ServerData/");

        path += platform;

        int filesRenamed = 0;
        foreach (string file in System.IO.Directory.GetFiles(path))
        {
            if (!System.IO.Path.GetFileName(file).Contains("catalog_") || System.IO.Path.GetFileName(file).Contains(platform))
            {
                string newFileName = file.Replace(System.IO.Path.GetExtension(file), $"_{platform}{System.IO.Path.GetExtension(file)}");

                System.IO.File.Move(file, newFileName);

                filesRenamed++;
            }
        }
        EditorApplication.Beep();
        EditorUtility.DisplayDialog("Asset Bundle Renamer", $"Added the plattform extension {platform} to {filesRenamed} files in {path} that did not allready contain {platform}", "Great!");
    }
}