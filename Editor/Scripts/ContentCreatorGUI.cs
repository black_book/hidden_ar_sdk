using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BlackBook.Unity_Utilities;
using Submodules.SDK.Editor.Scripts;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

namespace BlackBook.HiddenEditor.SDK
{
    public static class SDKConst
    {
        public const string BUCKET_NAME = "hidden-ar-solution-assetbundles";
        public const string SERVER_LOCATION = "eu-central-1";
    }

    public class ContentCreatorGUI : EditorWindow
    {
        #region Variables

        private string access;
        private string secret;

        string accessKeyUserInput = "Access Key";
        string secretKeyUserInput = "Secret Key";
        string createAndUploadAssetBundle = "Create Asset Bundle and Upload";
        string clearCredentialsLabel = "Clear Credentials";
        string instructions =
            "Welcome to the Hidden SDK UPLOAD TOOL! \n\n INSTRUCTIONS  \n\n 1. Fill Out the credentials.  \n\n 2. Create a new 'CONTENT PACK' by" +
            " Create -> Black Book -> New Content Pack. Fill it out and populate the 'settings' slot." +
            " \n\n 3. Click on 'Create Asset Bundle and Upload' and check console after for any errors . \n\n IMPORTANT NOTE: YOU DO NOT NEED TO RE-ENTER YOUR CREDENTIALS IF IT WORKED FOR THE FIRST TIME";

        private ExperienceData experienceData;
        public Object settingsGO;
        private ContentPack contentPack;

        private string editorKeyPrefix = "AssetUploadTool";

        #endregion

        [MenuItem("Hidden SDK/Upload Asset Tool")]
        private static void ShowWindow()
        {
            var window = GetWindow<ContentCreatorGUI>();
            window.titleContent = new GUIContent("Upload Asset Tool");
            window.Show();
        }

        private void OnGUI()
        {
            GUILayout.Space(10);
            GUILayout.Label("Credentials");

            if (EditorPrefs.HasKey(editorKeyPrefix + "ACCESS") && EditorPrefs.HasKey(editorKeyPrefix + "SECRET"))
            {
                accessKeyUserInput = EditorPrefs.GetString(editorKeyPrefix + "ACCESS");
                secretKeyUserInput = EditorPrefs.GetString(editorKeyPrefix + "SECRET");
            }
            else
            {
                accessKeyUserInput = GUILayout.TextField(accessKeyUserInput);
                secretKeyUserInput = GUILayout.TextField(secretKeyUserInput);
            }

            GUILayout.Space(10);
            GUILayout.Label("Settings");
            settingsGO = EditorGUILayout.ObjectField(settingsGO, typeof(ContentPack), false);

            GUILayout.Space(10);

            if (settingsGO != null)
            {
                contentPack = (ContentPack)settingsGO;
                GUILayout.Label($"Scene Orientation: {contentPack.sceneOrientation}");
            }

            GUILayout.Space(10);

            if (GUILayout.Button(createAndUploadAssetBundle))
            {
                contentPack = (ContentPack)settingsGO;

                // Ensure the selected orientation is set in the content pack and valid
                bool contentPackValidation = CheckContentPackIsValid(contentPack);
                if (!contentPackValidation)
                {
                    Debug.LogError("Content pack is not valid. At least one platform should be selected and Scene Orientation should be valid.");
                    return;
                }

                if (accessKeyUserInput != "Access Key" || secretKeyUserInput != "Secret Key")
                {
                    EditorPrefs.SetString(editorKeyPrefix + "ACCESS", accessKeyUserInput);
                    EditorPrefs.SetString(editorKeyPrefix + "SECRET", secretKeyUserInput);
                    access = accessKeyUserInput;
                    secret = secretKeyUserInput;
                    Debug.Log("Your credentials have been saved.");
                }
                else
                {
                    access = PlayerPrefs.GetString("ACCESS");
                    secret = PlayerPrefs.GetString("SECRET");
                    if (access == "Access Key" || secret == "Secret Key")
                    {
                        Debug.LogError("Your credentials are invalid.");
                        return;
                    }
                }

                CreateAssetBundles.BuildAllAssetBundles(contentPack);
                MainThread();
            }

            GUILayout.Space(10);
            GUIStyle style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter };
            GUILayout.Box(instructions, GUILayout.ExpandWidth(true));

            if (GUILayout.Button(clearCredentialsLabel))
            {
                accessKeyUserInput = "Access Key";
                secretKeyUserInput = "Secret Key";
                EditorPrefs.DeleteKey(editorKeyPrefix + "ACCESS");
                EditorPrefs.DeleteKey(editorKeyPrefix + "SECRET");
                Debug.Log("All credentials have been reset.");
            }
        }

        private async void MainThread()
        {
            // Initialize Data Access Layer
            DataAccess.InitializeDataAccess(access, secret);
            experienceData = await DataAccess.GetExperienceData(contentPack.writeKey);

            if (experienceData != null)
            {
                Debug.Log("Experience Data retrieved from DynamoDB.");

                if (!string.IsNullOrEmpty(experienceData.assetURL))
                {
                    List<string> properLinks = GetProperLinks(experienceData);
                    List<string> deletableKeys = GetDeletableKeys(properLinks);
                    DataAccess.DeleteAssetBundlesInSlot(deletableKeys, OnAssetBundleDeletedInS3);
                }
                else
                {
                    Debug.Log("No asset bundle URL found, creating new attributes in DynamoDB.");
                    // Pass sceneOrientation when creating attributes, convert enum to string
                    DataAccess.CreateAttributes(contentPack.ownersName, experienceData.id, contentPack.sceneOrientation.ToString());
                    OnAssetBundleDeletedInS3();
                }
            }
            else
            {
                Debug.LogError("Experience data not found in DynamoDB. Check the WriteKey.");
                TerminateUploadTool();
            }
        }

        #region Affirmative Callbacks

        private void OnAssetBundleDeletedInS3()
        {
            Debug.Log("Previous assets deleted from S3. Uploading new bundle.");
            List<UploadContentObject> list = PrepareDataToUpload();
            DataAccess.UploadAssetBundlesInSlot(list, UpdateDynamo);
        }

        private void UpdateDynamo()
        {
            Debug.Log("Updating DynamoDB.");
            // Convert SceneOrientation enum to string before passing
            string customURL = $"https://{SDKConst.BUCKET_NAME}.s3.{SDKConst.SERVER_LOCATION}.amazonaws.com/LIVE/{contentPack.ownersName}/{contentPack.referenceName}_[PLATFORM].bundle";
            DataAccess.UpdateURLInDynamoDB(customURL, contentPack.ownersName, experienceData.id, contentPack.sceneOrientation.ToString());
        }

        #endregion

        #region Negative Callbacks

        private void TerminateUploadTool()
        {
            EditorApplication.isPlaying = false;
        }

        #endregion

        #region Operations

        private List<string> GetProperLinks(ExperienceData experienceData)
        {
            List<string> links = new List<string>();
            string PLATFORM_REGEX = @"\[(.*?)\]";
            var android = Regex.Replace(experienceData.assetURL, PLATFORM_REGEX, "Android");
            var ios = Regex.Replace(experienceData.assetURL, PLATFORM_REGEX, "iOS");

            if (contentPack.android && !contentPack.iOS)
            {
                links.Add(android);
            }
            else if (!contentPack.android && contentPack.iOS)
            {
                links.Add(ios);
            }
            else if (contentPack.android && contentPack.iOS)
            {
                links.Add(android);
                links.Add(ios);
            }

            return links;
        }

        private List<string> GetDeletableKeys(List<string> links)
        {
            List<string> keyNames = new List<string>();
            foreach (string s in links)
            {
                string regex = $"(?<={contentPack.ownersName}/).*";
                Match bundleNameWithExtension = Regex.Match(s, regex, RegexOptions.IgnoreCase);
                if (bundleNameWithExtension.Success)
                {
                    string bundleName = $"LIVE/{contentPack.ownersName}/{bundleNameWithExtension.Value}";
                    keyNames.Add(bundleName);
                }
            }
            return keyNames;
        }

        private List<UploadContentObject> PrepareDataToUpload()
        {
            List<UploadContentObject> uploadContentObjects = new List<UploadContentObject>();

            if (contentPack.android)
            {
                string newName = contentPack.referenceName + "_Android";
                AssetDatabase.RenameAsset("Assets/Resources/AssetBundles/uploadassetlabel-Android", newName);
                UploadContentObject android = new UploadContentObject
                {
                    FilePath = "Assets/Resources/AssetBundles/" + newName,
                    KeyName = "LIVE/" + contentPack.ownersName + "/" + contentPack.referenceName + "_Android.bundle",
                    Metadata = new Dictionary<string, string>
                    {
                        { "SceneOrientation", contentPack.sceneOrientation.ToString() }  // Convert enum to string
                    }
                };
                uploadContentObjects.Add(android);
            }

            if (contentPack.iOS)
            {
                string newName = contentPack.referenceName + "_iOS";
                AssetDatabase.RenameAsset("Assets/Resources/AssetBundles/uploadassetlabel-iOS", newName);
                UploadContentObject iosObj = new UploadContentObject
                {
                    FilePath = "Assets/Resources/AssetBundles/" + newName,
                    KeyName = "LIVE/" + contentPack.ownersName + "/" + contentPack.referenceName + "_iOS.bundle",
                    Metadata = new Dictionary<string, string>
                    {
                        { "SceneOrientation", contentPack.sceneOrientation.ToString() }  // Convert enum to string
                    }
                };
                uploadContentObjects.Add(iosObj);
            }

            return uploadContentObjects;
        }

        public static bool CheckContentPackIsValid(ContentPack contentPack)
        {
            if (contentPack != null)
            {
                if (!string.IsNullOrEmpty(contentPack.writeKey) && contentPack.unityScene != null)
                {
                    if (contentPack.android || contentPack.iOS)
                    {
                        if (!string.IsNullOrEmpty(contentPack.sceneOrientation.ToString()))
                        {
                            return true;
                        }
                        else
                        {
                            Debug.LogError("Invalid scene orientation selected!");
                        }
                    }
                    else
                    {
                        Debug.LogError("Please select at least one platform (Android or iOS)!");
                    }
                }
                else
                {
                    Debug.LogError("Please fill out the write key and Unity scene slot in the content creation package!");
                }
            }
            else
            {
                Debug.LogError("Content pack is null!");
            }
            return false;
        }

        #endregion
    }
}
