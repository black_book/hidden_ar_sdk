using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Ludiq;
using UnityEngine.Events;

namespace BlackBook.HiddenEditor
{

    //[InitializeOnLoad]
    public class BoltIncludeSync
    {

        static float saveTime = 300.0f;
        static double nextSave = 0;


        static BoltIncludeSync()
        {
            EditorApplication.update += AutoSync;

        }

        private static void AutoSync()
        {

            if (EditorApplication.isPlaying) return;
            double timeToSave = nextSave - EditorApplication.timeSinceStartup;

            if (EditorApplication.timeSinceStartup > nextSave)
            {
                nextSave = EditorApplication.timeSinceStartup + saveTime;
                //EditorApplication.update -= AutoSync;
                Sync(() =>
                {
                    Debug.Log($"Bolt configs succesfully synced from SDK to Project. Remember to Tools > Bolt > Build Unit Options :)");
//TODO: When in a proper code editor, test if UnitBase.Build() is the command for building unit options. It is according to people on the bolt discod

                }, () => { Debug.LogWarning("Bolt Sync failed"); }, () => { Debug.LogWarning("Bolt Sync failed, no source file"); });
            }
        }


        [MenuItem("Hidden SDK/Sync Bolt Includes")]
        public static void ManualSync()
        {
            EditorUtility.DisplayDialog("Sync Bolt Includes",
                $"Sync the bolt configs between the authorative source and this project? " +
                $"There is no reason not to do this.", "Yes!");

            Sync(() =>
            {
                Debug.Log("Copied Ludiq Settings from SDK");
                EditorUtility.DisplayDialog("Sync Bolt Includes", $"Bolt configs succesfully synced from SDK to Project. Remember to Tools > Bolt > Build Unit Options :)", "Cool");
            },
            () =>
            {

                EditorUtility.DisplayDialog("Sync Bolt Includes",
                        $"Something went wrong when syncining bolt config from the SDK to this project. " +
                        $"See the Console for more details.", "Not ideal");
            },
            () =>
            {
                EditorUtility.DisplayDialog("Sync Bolt Includes",
                $"No source file present in SDK to sync from, update the SDK. " +
                $"If problem persist contact the developers as there may be an issue with the source file",
                "Ok");
            }
            );
        }
        public static void Sync(UnityAction onSuccess, UnityAction onBoltError, UnityAction onMissingSourceFile)
        {
            //If this IS the authorative project, we ignore all of this code!
            if (Application.productName == "Hidden AR")
            {
                return;
            }

            object o = Resources.Load("LudiqGenerated");

            if (o != null)
            {
                try
                {
                    DictionaryAsset da = o as DictionaryAsset;
                    if (da != null)
                    {
                        List<LooseAssemblyName> assemblies = (List<LooseAssemblyName>)da["assemblyOptions"];
                        List<System.Type> types = (List<System.Type>)da.dictionary["typeOptions"];

                        //LudiqCore.Configuration.SaveProjectSettingsAsset();
                        LudiqCore.Configuration.assemblyOptions.Clear();
                        LudiqCore.Configuration.assemblyOptions.AddRange(assemblies);

                        LudiqCore.Configuration.typeOptions.Clear();
                        LudiqCore.Configuration.typeOptions.AddRange(types);

                    }
                }
                catch (System.Exception e)
                {
                    onBoltError?.Invoke();
                    Debug.LogError("Sync ERROR: " + e.Message);
                    return;

                }
                onSuccess?.Invoke();
            }
            else
            {

                onMissingSourceFile?.Invoke();

            }
        }
    }
}

