using UnityEngine;

public class ClickHandler : MonoBehaviour
{
    private Camera sceneCamera;

    private void Start()
    {
        // Get the only camera in the scene
        sceneCamera = Object.FindObjectOfType<Camera>();

        // Optional: Check if no camera is found and log a warning
        if (sceneCamera == null)
        {
            Debug.LogWarning("No camera found in the scene.");
        }
    }

    public bool CheckClickOrTap(Vector3 inputPosition)
    {
        if (sceneCamera == null)
        {
            Debug.LogError("Camera not assigned in ClickHandler.");
            return false;
        }

        // Convert screen point to ray
        Ray ray = sceneCamera.ScreenPointToRay(inputPosition);
        RaycastHit hit;

        // Check if the ray hits the object's collider
        if (Physics.Raycast(ray, out hit))
        {
            return hit.transform == transform;
        }
        return false;
    }
}
